#pragma once
#include <vector>

//Include default systems and components
#include "ecs/ECS.hpp"

//Include scene class and logger
#include "utils/Scene.hpp"
#include "utils/Logger.hpp"

using namespace std;

//Empty game, minimal example
class ExampleGame : public Scene{
    public:
    //Contructor of the game, resources passed through here won't be allocated yet, only in PostInit()
     ExampleGame(SceneGraph* _sg):
        Scene(_sg,0,new vector<pair<size_t,Resource*>>({}))
    {}

    //From Scene
    void PostInit() override{
        reloadSettings();
    }
    //Reload settings function, called after settings change
    void reloadSettings() override{

        //Unset dirty bit
        Settings::unsetDirty();
    }
    //Main loop, empty for now
    void Update(const float dt) override{}
    //For deallocating nonstandard resources, run before changing scenes
    void DeInit() override{}

};