#include <stdio.h>
#include <string>

#include "Game.hpp"
#include "SceneGraph.hpp"
#include "utils/Settings.hpp"
#include "utils/Resources.hpp"

using namespace std;

int main(int argc, char** argv){
    //Apply settings, mandatory variables
    Settings::set(Settings::FPS,"60"); //Maximum FPS
    Settings::set(Settings::SCREEN_WIDTH,"800"); //Width of default window
    Settings::set(Settings::SCREEN_HEIGHT,"600"); //Height of default window
    Settings::set(Settings::MEDIA_PATH,"data"); //Location of assets
    Settings::set(Settings::DEBUG_LEVEL,"0"); //Debug level, more information in utils/Resources.hpp

    //Engines
    Engine::Start<RenderEngine>(REOptions()); //Start rendering engine

    //Start resources
    if(!Resources::Initialize()){
        //If resources are not found or cant be allocated, make sure logger writes any messages and quit the program
        Logger::write();
        return 1;
    }
    //Create a scene graph
    SceneGraph graph;

    //Add node (scene)
    graph.Add(new ExampleGame(&graph));

    //Start Game (main loop), 0 is SceneID of ExampleGame - entry point
    graph.Loop(0);

    //Free resources
    Resources::Dealloc();

    return 0;
}