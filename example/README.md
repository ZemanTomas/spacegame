### Guide for examples (Not compatible with current version of the engine):
This directory contains several examples serving as demonstration of frameworks functionalities and partially as a manual on its usage. \
Directory data contains assets used in these examples.
- First example: Empty game, minimal example
- Second example: Creating a player
- Third example: Creating console
- Fourth example: Creating more physics objects
- Fifth example: Playing audio
- Sixth example: Creating GUI
- Seventh example: Animating sprites and scripting basics
- Final example: Mouse interactions, procedurally generated world

#### Building (CMake):

mkdir build
cd build
cmake ..
make [example{1,2,3,4,5,6,7}]

#### Building (CMake) - Windows (MINGW):

sudo apt-get install mingw-w64
mkdir build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE=../../mingw-w64-x86_64.cmake ..
make [example{1,2,3,4,5,6,7}]

#### Building (Makefile) - DEPRECATED:
Install SDL2 SDL2_image SDL2_mixer SDL2_ttf libraries. 

For Windows:
- make EXAMPLEDIRECTORY=<example_directory> win

or for Linux:
- make EXAMPLEDIRECTORY=<example_directory>

debug on Windows:
- make EXAMPLEDIRECTORY=<example_directory> debug_win

and for Linux:
- make EXAMPLEDIRECTORY=<example_directory> debug
