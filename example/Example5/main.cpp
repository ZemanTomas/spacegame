#include <stdio.h>
#include <string>

#include "Game.hpp"
#include "SceneGraph.hpp"
#include "utils/Settings.hpp"
#include "utils/Resources.hpp"

using namespace std;

int main(int argc, char** argv){
    //Apply settings, mandatory variables
    Settings::set(Settings::FPS, "60");
    Settings::set(Settings::SCREEN_WIDTH,"800");
    Settings::set(Settings::SCREEN_HEIGHT, "600");
    Settings::set(Settings::MEDIA_PATH, "data");
    Settings::set(Settings::FONT, "font.ttf");
    Settings::set(Settings::TEXTURE_PLAYER, "player.png");
    Settings::set(Settings::TEXTURE_MAP, "tiles.png");
    Settings::set(Settings::DEBUG_LEVEL, "0");
    Settings::set(Settings::MUSIC, "beat.wav");
    Settings::set(Settings::MUSIC_ON, "true");

    //Engines
    Engine::Start<RenderEngine>(REOptions());
    Engine::Start<SoundEngine>(SEOptions());

    //Start resources
    if(!Resources::Initialize()){
        Logger::write();
        return 1;
    }
    //Create a scene graph
    SceneGraph graph;

    //Add node (scene)
    graph.Add(new ExampleGame(&graph));

    //Start Game (main loop), 0 is SceneID of ExampleGame - entry point
    graph.Loop(0);

    //Free resources
    Resources::Dealloc();

    return 0;
}