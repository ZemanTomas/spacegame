#pragma once
#include <vector>

//Include default systems and components
#include "ecs/ECS.hpp"

//Include scene class and logger
#include "utils/Scene.hpp"
#include "utils/Logger.hpp"

using namespace std;

//Texture resource initialization
void* Init_TEXTURES(){
    try{
        vector<Texture*>* textures = new vector<Texture*>(Resources::TEXTURES_TOTAL);
        fs::path temp_player = Settings::getAs<fs::path>(Settings::MEDIA_PATH)/Settings::getAs<fs::path>(Settings::TEXTURE_PLAYER);
        (*textures)[Resources::PLAYER_TEXTURE] = new Texture();
        (*textures)[Resources::PLAYER_TEXTURE]->load(temp_player.string(),RenderEngine::getRenderer());
        fs::path temp_map = Settings::getAs<fs::path>(Settings::MEDIA_PATH)/Settings::getAs<fs::path>(Settings::TEXTURE_MAP);
        (*textures)[Resources::MAP_TEXTURE] = new Texture();
        (*textures)[Resources::MAP_TEXTURE]->load(temp_map.string(),RenderEngine::getRenderer());
        return textures;
    }catch(const exception&){//TODO Error message
        return NULL;
    }
}
//Font resource initialization
void* Init_FONT(){
    fs::path temp = Settings::getAs<fs::path>(Settings::MEDIA_PATH)/Settings::getAs<fs::path>(Settings::FONT);
    string fontPath(temp.string());
    return TTF_OpenFont(fontPath.c_str(),20);
        //Logger::logERROR("Failed to load font from :"+fontPath+" ,SDL_ttf Error: "+string(TTF_GetError()));
}

//Empty game, minimal example
class ExampleGame : public Scene{
    public:
    ExampleGame(SceneGraph* _sg):
        Scene(_sg,0,new vector<pair<size_t,Resource*>>({
            {
                Resources::TEXTURES,
                new Resource(
                    &Init_TEXTURES,
                    [](void* res){for(Texture* t : *static_cast<vector<Texture*>*>(res)){if(t!=NULL){delete t;}}},
                    [](void* res){return static_cast<vector<Texture*>*>(res) != NULL;}
                )
            },
            {
                Resources::FONT,
                new Resource(
                    &Init_FONT,
                    [](void* res){TTF_CloseFont(static_cast<TTF_Font*>(res));},
                    [](void* res){return static_cast<TTF_Font*>(res) != NULL;}
                )
            },
            {
                Resources::TYPES_TOTAL+1,
                new Resource(   []{return static_cast<void*>(Mix_LoadMUS((Settings::getAs<fs::path>(Settings::MEDIA_PATH)/Settings::getAs<fs::path>(Settings::MUSIC)).string().c_str()));},
                                [](void* res){Mix_FreeMusic(static_cast<Mix_Music*>(res));},
                                [](void* res){return res != NULL;})
            }
        })),
        music(INVALID_ENTITY)
    {
        Init();

        //Create Component storages
        manager.createComponentStore<PhysicsComponent>();
        manager.createComponentStore<TypeComponent>();
        manager.createComponentStore<InputComponent>();
        manager.createComponentStore<SpriteComponent>();
        manager.createComponentStore<ConsoleComponent>();
        manager.createComponentStore<GUIComponent>();
        manager.createComponentStore<SoundComponent>();

        //Commands for console
        consoleFunctions = {
            {"teleportPlayer",{[this](vector<string>& par){teleportPlayer(stof(par[0]),stof(par[1]));},2}},
            {"displayPosition",{[this](vector<string>& par){writePlayerPos();},0}}
        };

        //Create Systems
        manager.addSystem(System::SysPtr(new PhysicsSystem(manager)));
        manager.addSystem(System::SysPtr(new RenderSystem(manager,RenderEngine::getRenderer(),textures,&cam)));
        manager.addSystem(System::SysPtr(new ConsoleSystem(manager,&events,RenderEngine::getFont(),RenderEngine::getRenderer(),_sg,&consoleFunctions)));
        manager.addSystem(System::SysPtr(new PlayerInputSystem(manager,&events,&cam)));
        manager.addSystem(System::SysPtr(new GUISystem(manager)));
        manager.addSystem(System::SysPtr(new AudioSystem(manager)));

        //Create Console
        createConsole();
        
        //Create Box2D world
        world = new b2World({0.0f,0.0f});
        world->SetAllowSleeping(true);
        world->SetWarmStarting(true);
        
        //Create player
        player = manager.createEntity();
        float_2D size = {24.0f,24.0f};
        float_2D pos = {1000.0f,1000.0f};
        manager.addComponent(player,PhysicsComponent(world,pos,size,true,true,{0.0f,1.0f,0.1f,0.0f,15.0f}));
        manager.addComponent(player,InputComponent(true));
        manager.addComponent(player,TypeComponent(0));
        manager.addComponent(player,SpriteComponent(RenderEngine::getPlayerTexture(),{10,3,13,26}));
        manager.registerEntity(player);

        //Create box
        box = manager.createEntity();
        float_2D size_b = {34.0f,34.0f};
        float_2D pos_b = {1050.0f,1050.0f};
        manager.addComponent(box,PhysicsComponent(world,pos_b,size_b,false,false,{0.0f,1.0f,0.1f,0.0f,15.0f}));
        manager.addComponent(box,SpriteComponent(RenderEngine::getTextureMap(),{6*32,2*32,32,32}));
        manager.getComponent<SpriteComponent>(box).SetAnimation({32,32},{6,2},2);
        manager.registerEntity(box);
        
        //Create Music
        void* track1 = Resources::getInstance()[Resources::TYPES_TOTAL+1];
        music = manager.createEntity();
        manager.addComponent(music,SoundComponent(static_cast<Mix_Music*>(track1)));
        manager.registerEntity(music);
        
        //GUI
        window1 = manager.createEntity();
        manager.addComponent(window1,GUIComponent("Window1",InitGUI1));
        manager.registerEntity(window1);

        //Scripting
        Engine::getInstance<LuaEngine>().execute("print 'Hello lua!'");
    }

    //From Scene
    void PostInit() override{
        reloadSettings();
    }
    //Reload settings function, called after settings change
    void reloadSettings() override{
        //Camera size
        cam.size = {Settings::getAs<int32_t>(Settings::SCREEN_WIDTH),Settings::getAs<int32_t>(Settings::SCREEN_HEIGHT)};

        //Music
        if(music != INVALID_ENTITY){
            SoundComponent& snd = manager.getComponent<SoundComponent>(music);
            if(Settings::getAs<bool>(Settings::MUSIC_ON)){snd.action |= MUSIC_PLAY;}else{snd.action &= ~MUSIC_PLAY;}
        }

        //Unset dirty bit
        Settings::unsetDirty();
    }
    void Update(const float dt) override{
        //ECS
        manager.update(dt);

        //Physics
        world->Step(1/60.0f,6,3);

        //Update max speed
        PhysicsComponent& p = manager.getComponent<PhysicsComponent>(player);
        GUIComponent& w1 = manager.getComponent<GUIComponent>(window1);
        p.maxSpeed = *(float*)w1.values[0];

        //Update camera
        updateCamPos(dt,player,cam);
    }
    void DeInit() override{}

    vector<Texture*>* textures;

    //Camera:
    Camera cam;

    //Player
    Entity player;
    //Music
    Entity music;
    //Box
    Entity box;
    //GUI
    Entity window1;
    //Gui window1 function, defines how window looks like and what variables it retunrs, user data is an arbitrary pointer for parameter passing
    static vector<void*> InitGUI1(string name, [[maybe_unused]] void* user_data){
        static float slider1 = 15.0f;
        static float color[4] = {1.0f,1.0f,1.0f,1.0f};
        vector<void*> params = {&slider1,color};
        ImGui::Begin(name.c_str());
        ImGui::SliderFloat("maxSpeed", (float*)params[0],1.0f,40.0f);
        ImGui::ColorEdit3("blockColor",(float*)params[1]);//Currently unused
        ImGui::End();
        return params;
    }

    //Box2D World
    b2World* world;

    //Update cam position to center on player
     void updateCamPos([[maybe_unused]] const float dt, Entity player, Camera& cam){
        PhysicsComponent col = manager.getComponent<PhysicsComponent>(player);
        b2Vec2 pos = col.body->GetPosition();
        int_2D center = {(int)(pos.x*PPM),(int)(pos.y*PPM)};
        cam.pos = {center.x-(int)(cam.size.x/2.0f),center.y-(int)(cam.size.y/2.0f)};
    }

    
    //Console
    Entity console;
    map<string,pair<function<void(vector<string>&)>,size_t>> consoleFunctions;
    void createConsole(){
        console = manager.createEntity();
        manager.addComponent(console,InputComponent(false));
        manager.addComponent(console,ConsoleComponent(""));
        manager.registerEntity(console);
    }

    //Scene Console commands
    void teleportPlayer(float x, float y){
        PhysicsComponent& p = manager.getComponent<PhysicsComponent>(player);
        p.setPos({x,y});
    }
    void writePlayerPos(){
        PhysicsComponent& p = manager.getComponent<PhysicsComponent>(player);
        Logger::logINFO(to_string(p.getPos().x)+" "+to_string(p.getPos().y));
    }
};