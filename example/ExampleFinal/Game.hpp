#pragma once
#include <vector>
#include <functional>

//Include default systems and components
#include "ecs/ECS.hpp"
//Include user systems and components
#include "WorldComponent.hpp"
#include "WorldSystem.hpp"

#include "utils/Scene.hpp"
#include "utils/Logger.hpp"


using namespace std;


class SpaceGame : public Scene{
    public:
    SpaceGame(SceneGraph* _sg):
        Scene(_sg,0,new vector<pair<size_t,Resource*>>({
            {
                Resources::TYPES_TOTAL+1,
                new Resource(   []{return static_cast<void*>(Mix_LoadMUS((Settings::getAs<fs::path>(Settings::MEDIA_PATH)/Settings::getAs<fs::path>(Settings::MUSIC)).string().c_str()));},
                                [](void* res){Mix_FreeMusic(static_cast<Mix_Music*>(res));},
                                [](void* res){return res != NULL;})
            }
        })),
        textures(RenderEngine::getTextures()),
        renderer(RenderEngine::getRenderer()),
        font(RenderEngine::getFont()),
        world(INVALID_ENTITY),
        window1(INVALID_ENTITY),
        console(INVALID_ENTITY),
        music(INVALID_ENTITY)
    {
        //Create Component storages
        manager.createComponentStore<PhysicsComponent>();
        manager.createComponentStore<TypeComponent>();
        manager.createComponentStore<ConsoleComponent>();
        manager.createComponentStore<InputComponent>();
        manager.createComponentStore<SpriteComponent>();
        manager.createComponentStore<GUIComponent>();
        manager.createComponentStore<WorldComponent>();
        manager.createComponentStore<SoundComponent>();

        //Commands for console
        consoleFunctions = {
            {"teleportPlayer",{[this](vector<string>& par){teleportPlayer(stof(par[0]),stof(par[1]));},2}},
            {"save",{[this]([[maybe_unused]] vector<string>& par){saveWorld();},0}}
        };

        //Create Systems
        manager.addSystem(System::SysPtr(new PhysicsSystem(manager)));
        manager.addSystem(System::SysPtr(new RenderSystem(manager,renderer,textures,&cam)));
        manager.addSystem(System::SysPtr(new ConsoleSystem(manager,&events,font,renderer,_sg,&consoleFunctions)));
        manager.addSystem(System::SysPtr(new PlayerInputSystem(manager,&events,&cam)));
        manager.addSystem(System::SysPtr(new GUISystem(manager)));
        manager.addSystem(System::SysPtr(new WorldSystem(manager)));
        manager.addSystem(System::SysPtr(new AudioSystem(manager)));

        //Create Console
        createConsole();

        //GUI
        window1 = manager.createEntity();
        manager.addComponent(window1,GUIComponent("Window1",InitGUI1));
        manager.registerEntity(window1);

        //World
        world = manager.createEntity();
        manager.addComponent(world,WorldComponent(manager,100,100,&events,cam));
        manager.registerEntity(world);
        WorldComponent& w = manager.getComponent<WorldComponent>(world);
        w.createPlayer({100.5f*CHUNK_SIZE,100.5f*CHUNK_SIZE});
        w.LoadStart();

        //Scripting
        Engine::getInstance<LuaEngine>().execute("print 'Hello lua!'");
    }
    vector<Texture*>* textures;
    SDL_Renderer* renderer;
    TTF_Font* font;

    //Camera:
    Camera cam;

    //World
    Entity world;

    //GUI
    Entity window1;
    static vector<void*> InitGUI1(string name, [[maybe_unused]] void* user_data){
        static float slider1 = 15.0f;
        static float color[4] = {1.0f,1.0f,1.0f,1.0f};
        vector<void*> params = {&slider1,color};
        ImGui::Begin(name.c_str());
        ImGui::SliderFloat("maxSpeed", (float*)params[0],1.0f,40.0f);
        ImGui::ColorEdit3("blockColor",(float*)params[1]);
        ImGui::End();
        return params;
    }

    //Console
    Entity console;
    map<string,pair<function<void(vector<string>&)>,size_t>> consoleFunctions;
    void createConsole(){
        console = manager.createEntity();
        manager.addComponent(console,InputComponent(false));
        manager.addComponent(console,ConsoleComponent(""));
        manager.registerEntity(console);
    }

    //Music
    Entity music;

    //From Scene
    void PostInit() override{//TODO completely set up new scene
        //Player reset
        WorldComponent& w = manager.getComponent<WorldComponent>(world);
        PhysicsComponent& p = manager.getComponent<PhysicsComponent>(w.player);
        p.body->SetTransform({100.5f*CHUNK_SIZE,100.5f*CHUNK_SIZE},0);
        p.body->SetLinearVelocity({0,0});

        //Music
        void* track1 = Resources::getInstance()[(*res)[0].first];
        music = manager.createEntity();
        manager.addComponent(music,SoundComponent(static_cast<Mix_Music*>(track1)));
        manager.registerEntity(music);

        reloadSettings();
    }
    void reloadSettings() override{
        //Camera size
        cam.size = {Settings::getAs<int32_t>(Settings::SCREEN_WIDTH),Settings::getAs<int32_t>(Settings::SCREEN_HEIGHT)};

        //Music
        if(music != INVALID_ENTITY){
            SoundComponent& snd = manager.getComponent<SoundComponent>(music);
            if(Settings::getAs<bool>(Settings::MUSIC_ON)){snd.action |= MUSIC_PLAY;}else{snd.action &= ~MUSIC_PLAY;}
        }

        Settings::unsetDirty();
    }
    void Update(const float dt) override{
        manager.update(dt);

        //Update player max speed
        WorldComponent& w = manager.getComponent<WorldComponent>(world);
        PhysicsComponent& p = manager.getComponent<PhysicsComponent>(w.player);
        GUIComponent& w1 = manager.getComponent<GUIComponent>(window1);
        p.maxSpeed = *(float*)w1.values[0];

        //Load chunks where the player is every 60 ticks
        static uint64_t index = 0;
        if(++index % 60 == 0){
            uint32_t cx = ((int32_t)p.getPos().x < 0 ? UINT32_MAX - (int32_t)p.getPos().x : (int32_t)p.getPos().x)/CHUNK_SIZE;
            uint32_t cy = ((int32_t)p.getPos().y < 0 ? UINT32_MAX - (int32_t)p.getPos().y : (int32_t)p.getPos().y)/CHUNK_SIZE;

            w.setMid(cx,cy);
        }
    }
    void DeInit() override{}

    //Console
    void teleportPlayer(float x, float y){
        WorldComponent& w = manager.getComponent<WorldComponent>(world);
        PhysicsComponent& p = manager.getComponent<PhysicsComponent>(w.player);
        p.setPos({x,y});
    }
    void saveWorld(){
        WorldComponent& w = manager.getComponent<WorldComponent>(world);
        w.saveWorld();
    }

};