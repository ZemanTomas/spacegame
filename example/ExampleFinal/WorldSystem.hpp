#pragma once
#include <map>
#include <array>
#include <random>
#include <algorithm>

#include <SDL.h>
#include "ecs/Manager.hpp"
#include "WorldComponent.hpp"

#include "engines/RenderEngine.hpp"

using namespace std;

class WorldSystem : public System{
    public:
    WorldSystem(Manager& _m):System(_m){
        set<ComponentType> req;
        //Insert required components identificators
        req.insert(WorldComponent::ctype);
        setRequiredComponents(move(req));
    }
    //Mandatory override from System, dt - time elapsed since last tick, entity _e is guaranteed to contain components set in constructor as required
    virtual void updateEntity(float dt, Entity _e){
        WorldComponent& w = manager.getComponent<WorldComponent>(_e);
        w.world->Step(1/60.0f,6,3);
        
        //TODO to function
        int mouseX, mouseY;
        const int buttons = SDL_GetMouseState(&mouseX,&mouseY);
		bool bl = buttons & SDL_BUTTON(SDL_BUTTON_LEFT);
		bool br = buttons & SDL_BUTTON(SDL_BUTTON_RIGHT);
        static bool old_bl = false,old_br = false;
        
        uint32_t mx = static_cast<int>(mouseX)+w.cam.pos.x;
        uint32_t my = static_cast<int>(mouseY)+w.cam.pos.y;
        uint32_t rx = ceilf(mx/PPM);
        uint32_t ry = ceilf(my/PPM);
        uint32_t x = ((uint32_t)rx>>CHUNK_BITS) - (w.midx - ACD/2);
        uint32_t y = ((uint32_t)ry>>CHUNK_BITS) - (w.midy - ACD/2);
        uint8_t bx = (uint8_t)ceilf((mx-(x+w.midx-ACD/2)*CHUNK_SIZE*PPM+(uint32_t)PPM/2)/PPM)-1;
        uint8_t by = (uint8_t)ceilf((my-(y+w.midy-ACD/2)*CHUNK_SIZE*PPM+(uint32_t)PPM/2)/PPM)-1;
        if(Settings::getDEBUG_LEVEL() > 4){
            Logger::logINFO("Mouse "+to_string(static_cast<int>(mouseX))+" Cam "+to_string(w.cam.pos.x)+" mx "+
                                     to_string(mx)+" rx "+to_string(rx)+" midx "+to_string(w.midx)+" xy "+
            to_string(x)+" "+to_string(y)+":"+to_string(bx)+" "+to_string(by));
        }
        if((x < ACD) && (y < ACD)){
            //Left click
            if(bl && !old_bl){
                old_bl = bl;
                w.incrementBlock(x,y,bx,by);
            }else if(!bl && old_bl){
                old_bl = bl;
            }
            //Right click
            if(br && !old_br){
                old_br = br;
                w.decrementBlock(x,y,bx,by);
            }else if(!br && old_br){
                old_br = br;
            }
        }

        updateCamPos(dt,w.player,w.cam);
        
    }
    //Update cam position to center on player
    void updateCamPos([[maybe_unused]] const float dt, Entity player, Camera& cam){//TODO size function
        PhysicsComponent col = manager.getComponent<PhysicsComponent>(player);
        b2Vec2 pos = col.body->GetPosition();
        int_2D center = {(int)(pos.x*PPM),(int)(pos.y*PPM)};
        cam.pos = {center.x-(int)(cam.size.x/2.0f),center.y-(int)(cam.size.y/2.0f)};
    }

};