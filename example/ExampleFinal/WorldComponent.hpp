#pragma once
#include <map>
#include <array>
#include <random>
#include <algorithm>

#include "ecs/Manager.hpp"
#include "utils/Utils.hpp"

#include "engines/FileEngine.hpp"

#define ACD 3
#define CHUNK_SIZE 64
#define CHUNK_BITS 6

using namespace std;

struct WorldOptions{
    WorldOptions(int _seed = 0):seed(_seed){}
    int seed;
};

struct Block{
    Block():type(0),state(0){}
    Block(uint64_t _type):type(_type),state(0){}
    Block(uint64_t _type,uint64_t _state):type(_type),state(_state){}
    uint64_t type; //ModID/BlockID
    uint64_t state;
};

struct Chunk{//CHUNK_SIZE*CHUNK_SIZE
    //Chunk* l,r,u,d;
    Chunk():state(0){}
    Chunk(uint64_t _state):state(_state){}
    uint64_t state;
    map<pair<uint8_t,uint8_t>,Block> blocks;

    Block& at(uint8_t x,uint8_t y){return blocks.at(make_pair(x,y));}
    void set(uint8_t x,uint8_t y,Block b){blocks[make_pair(x,y)] = b;}
};

struct ActiveChunk{
    ActiveChunk():block(),blocke(),state(0){
        //Logger::logINFO("AC Default Constructor");
    }
    ActiveChunk(Chunk& c,uint32_t _x, uint32_t _y, Manager* _m, b2World* _w):block(),blocke(),x(_x),y(_y),state(c.state),m(_m),w(_w){
        //Logger::logINFO("AC Constructor "+to_string(_x)+" "+to_string(_y));
        for(auto& b : c.blocks){
            if(b.second.type == 0){
                block[(b.first.second<<CHUNK_BITS)+b.first.first]=b.second;
                continue;
            }
            //Logger::logINFO("LoadingBlock "+to_string(b.first.first)+" "+to_string(b.first.second));
            createBlock(b.first.first,b.first.second,b.second);
        }
    }
    array<Block,CHUNK_SIZE*CHUNK_SIZE> block;
    array<Entity,CHUNK_SIZE*CHUNK_SIZE> blocke;
    uint32_t x,y;
    uint64_t state;
    Manager* m;
    b2World* w;

    Block& at(uint8_t x,uint8_t y){return block[((size_t)y<<CHUNK_BITS)+x];}
    Entity& ate(uint8_t x,uint8_t y){return blocke[((size_t)y<<CHUNK_BITS)+x];}
    Chunk save(){
        //Logger::logINFO("AC Save");
        Chunk c(state);
        for(uint16_t y = 0; y < CHUNK_SIZE; ++y){
            for(uint16_t x = 0; x < CHUNK_SIZE; ++x){
                if(block[((size_t)y<<CHUNK_BITS)+x].type != 0){c.set((uint8_t)x,(uint8_t)y,block[((size_t)y<<CHUNK_BITS)+x]);}
            }
        }
        return c;
    }
    void createBlock(uint32_t bx, uint32_t by, Block b = Block(2)){
        //Logger::logINFO("CreatingBlock "+to_string(bx)+" "+to_string(by)+" "+to_string(type)+" "+to_string(x)+" "+to_string(y));
        float_2D pos = {(float)(bx+x*CHUNK_SIZE)*PPM,(float)(by+y*CHUNK_SIZE)*PPM};
        float_2D size = {1.0f*PPM,1.0f*PPM};
        Entity e = m->createEntity();
        m->addComponent(e,PhysicsComponent(w,pos,size,false));
        //m->addComponent(e,SpriteComponent(0xA00000FF));
        if(b.type == 2){
            m->addComponent(e,SpriteComponent(RenderEngine::getTextureMap(),{6*32,2*32,32,32}));
            m->getComponent<SpriteComponent>(e).SetAnimation({32,32},{6,2},2);
        }else{
            m->addComponent(e,SpriteComponent(RenderEngine::getTextureMap(),{5*32,2*32,32,32}));
        }

        m->registerEntity(e);
        blocke[(by<<CHUNK_BITS)+bx] = e;
        block[(by<<CHUNK_BITS)+bx] = b;
    }
    ~ActiveChunk(){
        if(state == 0){return;}
        for(Entity& e : blocke){
            if(e != 0){
                PhysicsComponent& body = m->getComponent<PhysicsComponent>(e);
                w->DestroyBody(body.body);
                m->unregisterEntity(e);
            }
        }
    }
    
};
//World Component
struct WorldComponent : public Component{
    static const ComponentType ctype;
    public:
    WorldComponent(Manager& _m, uint32_t _midx, uint32_t _midy, vector<SDL_Event>* _e,Camera& _c, [[maybe_unused]] WorldOptions opt = WorldOptions()):
        midx(_midx),
        midy(_midy),
        oldx(_midx),
        oldy(_midy),
        events(_e),
        cam(_c),
        manager(_m)
    {
        world = new b2World({0.0f,0.0f});
        world->SetAllowSleeping(true);
        world->SetWarmStarting(true);
        activeChunks.fill(nullptr);

        loadFromFile();
        //Logger::logINFO("World Constructed!");
    }
    WorldComponent(WorldComponent&& w)noexcept:
        chunks(move(w.chunks)),
        activeChunks(move(w.activeChunks)),
        midx(w.midx),
        midy(w.midy),
        oldx(w.oldx),
        oldy(w.oldy),
        events(w.events),
        cam(w.cam),
        world(w.world),
        manager(w.manager),
        player(w.player),
        testbox(w.testbox)
    {w.world = nullptr;}
    map<pair<uint32_t,uint32_t>,Chunk> chunks;
    array<ActiveChunk*,ACD*ACD> activeChunks;//TODO on stack
    uint32_t midx,midy,oldx,oldy;

    vector<SDL_Event>* events;
    Camera& cam;

    b2World* world;
    Manager& manager;

    Chunk& at(uint32_t x,uint32_t y){return chunks.at(make_pair(x,y));}
    Block& at(uint32_t x,uint32_t y,uint8_t bx,uint8_t by){return at(x,y).at(bx,by);}

    void setMid(uint32_t x, uint32_t y){
        oldx = midx;
        oldy = midy;
        midx = x;
        midy = y;
        LoadAround();
    }
    void LoadAround(){
        int64_t dx = (int64_t)oldx - (int64_t)midx;
        int64_t dy = (int64_t)oldy - (int64_t)midy;
        if((dx == 0) && (dy == 0)){return;}
        //for old ones
        for(uint32_t y = 0; y < ACD ; ++y){
            for(uint32_t x = 0; x < ACD ; ++x){
                uint32_t cx = oldx - ACD/2 + x;
                uint32_t cy = oldy - ACD/2 + y;
                if((cx >= (midx - ACD/2) && cx <= (midx + ACD/2)) && (cy >= (midy - ACD/2) && cy <= (midy + ACD/2))){continue;}

                chunks[make_pair(cx,cy)] = activeChunks[y*ACD+x]->save();
                //Logger::logINFO("Delete " + to_string(cx) + " " + to_string(cy) + " : "+ to_string(x) + " " + to_string(y));
                delete activeChunks[y*ACD+x];
                activeChunks[y*ACD+x] = nullptr;
            }
        }
        if((ACD > 1) && (abs(dx) < ACD) && (abs(dy) < ACD)){
            if(dx < 0){
                shiftLeft(abs(dx));
            }else if(dx > 0){
                shiftRight(dx);
            }
            if(dy < 0){
                shiftUp(abs(dy));
            }else if(dy > 0){
                shiftDown(dy);
            }
        }
        //For new ones
        for(uint32_t y = 0; y < ACD ; ++y){
            for(uint32_t x = 0; x < ACD ; ++x){
                uint32_t cx = midx - ACD/2 + x;
                uint32_t cy = midy - ACD/2 + y;
                if(activeChunks[y*ACD+x] != nullptr){continue;}
                //if(!((cx >= (oldx - ACD/2) && cx <= (oldx + ACD/2)) && (cy >= (oldy - ACD/2) && cy <= (oldy + ACD/2)))){continue;}

                if(!chunks.contains(make_pair(cx,cy))){GenerateChunk(cx,cy);}
                //Logger::logINFO("Create " + to_string(cx) + " " + to_string(cy) + " : "+ to_string(x) + " " + to_string(y));
                activeChunks[y*ACD+x] = new ActiveChunk(at(cx,cy),cx,cy,&manager,world);
            }
        }
    }

    void GenerateChunk(uint32_t cx, uint32_t cy, [[maybe_unused]] int seed = 0){
        //Logger::logINFO("GenerateChunk "+to_string(cx)+" "+to_string(cy));
        Chunk c;c.state = 1;
        for(uint16_t y = 0; y < CHUNK_SIZE-0; ++y){
            for(uint16_t x = 0; x < CHUNK_SIZE-0; ++x){
                if(x % 4 == 0 && y % 4 == 0){
                    //Logger::logINFO("GenerateBlock "+to_string(x)+" "+to_string(y));
                    if((cx+cy) % 2 == 0){
                        c.set(x,y,Block(3));
                    }else{
                        c.set(x,y,Block(2));
                    }
                }
            }
        }
        chunks[make_pair(cx,cy)] = c;
    }

    //Player
    Entity player;
    Entity testbox;
    void createBlock(uint32_t x, uint32_t y, uint32_t bx, uint32_t by, uint8_t type = 2){
        activeChunks[y*ACD+x]->createBlock(bx,by,type);
    }
    void incrementBlock(uint32_t x, uint32_t y, uint8_t bx, uint8_t by){
        Block& b = activeChunks[y*ACD+x]->at(bx,by);
        Entity& e = activeChunks[y*ACD+x]->ate(bx,by);
        if(b.type != 0){
            if(b.type == 2){
                b.type = 3;
                manager.getComponent<SpriteComponent>(e).src = {5*32,2*32,32,32};
                manager.getComponent<SpriteComponent>(e).UnsetAnimation();
            }else{
                b.type = 0;
                PhysicsComponent& body = manager.getComponent<PhysicsComponent>(e);
                world->DestroyBody(body.body);
                manager.unregisterEntity(e);
                e = 0;
            }
        }else{
            createBlock(x,y,bx,by,2);
        }
    }
    void decrementBlock(uint32_t x, uint32_t y, uint8_t bx, uint8_t by){
        Block& b = activeChunks[y*ACD+x]->at(bx,by);
        Entity& e = activeChunks[y*ACD+x]->ate(bx,by);
        if(b.type != 0){
            if(b.type == 3){
                b.type = 2;
                manager.getComponent<SpriteComponent>(e).src = {6*32,2*32,32,32};
                manager.getComponent<SpriteComponent>(e).SetAnimation({32,32},{6,2},2);
            }else{
                b.type = 0;
                PhysicsComponent& body = manager.getComponent<PhysicsComponent>(e);
                world->DestroyBody(body.body);
                manager.unregisterEntity(e);
                e = 0;
            }
        }else{
            createBlock(x,y,bx,by,3);
        }
    }
    void createPlayer(float_2D pos){
        player = manager.createEntity();
        float_2D size = {24.0f,24.0f};
        manager.addComponent(player,PhysicsComponent(world,pos,size,true,true,{0.0f,1.0f,0.1f,0.0f,15.0f}));
        manager.addComponent(player,InputComponent(true));
        manager.addComponent(player,TypeComponent(0));
        manager.addComponent(player,SpriteComponent(RenderEngine::getPlayerTexture(),{10,3,13,26}));
        manager.registerEntity(player);
    }
    void shiftLeft(int32_t d){
        for(int y=0;y<ACD;++y){
            for(int x=0;x<ACD-d;++x){
                swap(activeChunks[y*ACD+x],activeChunks[y*ACD+x+1]);
            }
        }
    }
    void shiftRight(int32_t d){
        for(int y=0;y<ACD;++y){
            for(int x=ACD-d;x>0;--x){
                swap(activeChunks[y*ACD+x],activeChunks[y*ACD+x-1]);
            }
        }
    }
    void shiftUp(int32_t d){
        for(int y=0;y<ACD-d;++y){
            for(int x=0;x<ACD;++x){
                swap(activeChunks[y*ACD+x],activeChunks[y*ACD+x+ACD]);
            }
        }
    }
    void shiftDown(int32_t d){
        for(int y=ACD-d;y>0;--y){
            for(int x=0;x<ACD;++x){
                swap(activeChunks[y*ACD+x],activeChunks[y*ACD+x-ACD]);
            }
        }
    }
    

    void LoadStart(){
        for(uint32_t y = 0; y < ACD ; ++y){
            for(uint32_t x = 0; x < ACD ; ++x){
                uint32_t cx = midx - ACD/2 + x;
                uint32_t cy = midy - ACD/2 + y;
                if(!chunks.contains(make_pair(cx,cy))){GenerateChunk(cx,cy);}
                //Logger::logINFO("CreateN " + to_string(cx) + " " + to_string(cy) + " : "+ to_string(x) + " " + to_string(y));
                activeChunks[y*ACD+x] = new ActiveChunk(at(cx,cy),cx,cy,&manager,world);
            }
        }
    }
    
    //Load and save to file
    //Structure: {x y status nblocks(uint64_t) {block map}}
    void saveToFile(){
        size_t nbytes = 0;
        for(auto& c : chunks){
            nbytes+=2*sizeof(uint32_t)+2*sizeof(uint64_t);
            nbytes+=c.second.blocks.size()*(sizeof(Block)+2*sizeof(uint8_t));
        }
        //if(nbytes = 0){return;}
        char* serialized = (char*)malloc(nbytes);
        char* buf = serialized;

        for(auto& c : chunks){
            ((uint32_t*)buf)[0] = c.first.first;
            ((uint32_t*)buf)[1] = c.first.second;
            buf+=2*sizeof(uint32_t);
            ((uint64_t*)buf)[0] = c.second.state;
            ((uint64_t*)buf)[1] = c.second.blocks.size();
            buf+=2*sizeof(uint64_t);
            for(auto& b : c.second.blocks){
                ((uint8_t*)buf)[0] = b.first.first;
                ((uint8_t*)buf)[1] = b.first.second;
                buf+=2*sizeof(uint8_t);
                ((Block*)buf)[0] = b.second;
                buf+=sizeof(Block);
            }
        }

        FileEngine::saveBytes(serialized, nbytes, "world");
        free(serialized);
    }
    void loadFromFile(){
        map<pair<uint32_t,uint32_t>,Chunk> loadedChunks;
        int64_t size = FileEngine::fileSize("world");
        if(size >= 2*sizeof(uint32_t)+2*sizeof(uint64_t)){
            char* data = (char*)malloc(size+1);
            char* file = data;
            size_t loaded = FileEngine::loadBytes(file,"world");
            if(loaded != size){free(file);return;}
            while(loaded >= 2*sizeof(uint32_t)+2*sizeof(uint64_t)){
                //Load chunk
                uint32_t x = ((uint32_t*)file)[0];
                uint32_t y = ((uint32_t*)file)[1];
                file += sizeof(x)+sizeof(y);
                uint64_t state = ((uint64_t*)file)[0];
                uint64_t nblocks = ((uint64_t*)file)[1];
                file += sizeof(state)+sizeof(nblocks);
                loaded -= sizeof(state)+sizeof(nblocks)+sizeof(x)+sizeof(y);
                loadedChunks[make_pair(x,y)] = Chunk(state);
                //Load blocks
                if(loaded >= nblocks*(sizeof(Block)+2*sizeof(uint8_t))){
                    for(uint64_t i = 0; i < nblocks; i++){
                        uint8_t bx = ((uint8_t*)file)[0];
                        uint8_t by = ((uint8_t*)file)[1];
                        file += sizeof(bx)+sizeof(by);
                        Block b = ((Block*)file)[0];
                        file += sizeof(Block);
                        loadedChunks[make_pair(x,y)].set(bx,by,b);
                    }
                    loaded -= nblocks*(sizeof(Block)+2*sizeof(uint8_t));
                }
            }
            free(data);
            Logger::logINFO("Loaded "+to_string(loadedChunks.size()));
            chunks.swap(loadedChunks);
        }
        Logger::logINFO("EndLoading");

    }
    void saveWorld(){
        //Save to file
        for(ActiveChunk*& ac : activeChunks){
            if(ac != nullptr){
                chunks[make_pair(ac->x,ac->y)] = ac->save();
            }
        }
        saveToFile();
    }
    ~WorldComponent(){
        if(world){
            delete world;
        }
    }
};
//Required for each custom Component to guarantee unique identification
const ComponentType WorldComponent::ctype = __COUNTER__;