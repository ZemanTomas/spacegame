#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>

#include "Game.hpp"
#include "SceneGraph.hpp"
#include "utils/Settings.hpp"
#include "utils/Resources.hpp"
#include "utils/Getoptcpp.hpp"

using namespace std;

void* Init_FONT(){
    fs::path temp = Settings::getAs<fs::path>(Settings::MEDIA_PATH)/Settings::getAs<fs::path>(Settings::FONT);
    string fontPath(temp.string());
    return TTF_OpenFont(fontPath.c_str(),20);
        //Logger::logERROR("Failed to load font from :"+fontPath+" ,SDL_ttf Error: "+string(TTF_GetError()));
}
void* Init_TEXTURES(){
    try{
        vector<Texture*>* textures = new vector<Texture*>(Resources::TEXTURES_TOTAL);
        bool success;
        fs::path temp_player = Settings::getAs<fs::path>(Settings::MEDIA_PATH)/Settings::getAs<fs::path>(Settings::TEXTURE_PLAYER);
        fs::path temp_map = Settings::getAs<fs::path>(Settings::MEDIA_PATH)/Settings::getAs<fs::path>(Settings::TEXTURE_MAP);
        (*textures)[Resources::PLAYER_TEXTURE] = new Texture();
        (*textures)[Resources::MAP_TEXTURE] = new Texture();

        success = (*textures)[Resources::PLAYER_TEXTURE]->load(temp_player.string(),RenderEngine::getRenderer());
        if(!success){Logger::logERROR(string(SDL_GetError()));Logger::write();}
        
        success = (*textures)[Resources::MAP_TEXTURE]->load(temp_map.string(),RenderEngine::getRenderer());
        if(!success){Logger::logERROR(string(SDL_GetError()));Logger::write();}

        return textures;
    }catch(const exception&){//TODO Error message
        return NULL;
    }
}

int main(int argc, char** argv){
    argParser parser = argParser(argc,argv);
    parser.addOptionLong("fps",true,'f');
    parser.addOptionLong("width",true,'w');
    parser.addOptionLong("height",true,'h');
    parser.addOptionLong("verbose",false,'v');
    parser.addOptionLong("font",true,0);
    parser.addOptionLong("music",true,'m');
    parser.addOptionLong("nosound",false,'n');
    //Eval
    parser.eval();
    //Apply settings from args
    Settings::set(Settings::FPS, parser.isSet("fps") ? parser.get("fps") : "60");
    Settings::set(Settings::SCREEN_WIDTH, parser.isSet("width") ? parser.get("width") : "800");
    Settings::set(Settings::SCREEN_HEIGHT, parser.isSet("height") ? parser.get("height") : "600");
    Settings::set(Settings::MEDIA_PATH, "data");
    Settings::set(Settings::FONT, parser.isSet("font") ? parser.get("font") : "font.ttf");
    Settings::set(Settings::MUSIC, parser.isSet("music") ? parser.get("music") : "beat.wav");
    Settings::set(Settings::MUSIC_ON, parser.isSet("nosound") ? "false" : "true");
    Settings::set(Settings::TEXTURE_PLAYER, "player.png");
    Settings::set(Settings::TEXTURE_MAP, "tiles_small.png");
    Settings::set(Settings::DEBUG_LEVEL, "0");
    //Info
    if(parser.isSet("verbose")){
        //SDL2
        SDL_version compiled,linked;
        const SDL_version* linkedp;
        SDL_VERSION(&compiled);SDL_GetVersion(&linked);
        printf("We compiled against SDL version %d.%d.%d ...\n",compiled.major, compiled.minor, compiled.patch);
        printf("But we are linking against SDL version %d.%d.%d.\n",linked.major, linked.minor, linked.patch);
        //SDL2_ttf
        SDL_TTF_VERSION(&compiled);linkedp = TTF_Linked_Version();
        printf("We compiled against SDL_ttf version %d.%d.%d ...\n",compiled.major, compiled.minor, compiled.patch);
        printf("But we are linking against SDL_ttf version %d.%d.%d.\n",linkedp->major, linkedp->minor, linkedp->patch);
        //SDL2_mixer
        SDL_MIXER_VERSION(&compiled);linkedp = Mix_Linked_Version();
        printf("We compiled against SDL_mixer version %d.%d.%d ...\n",compiled.major, compiled.minor, compiled.patch);
        printf("But we are linking against SDL_mixer version %d.%d.%d.\n",linkedp->major, linkedp->minor, linkedp->patch);
        //SDL2_image
        SDL_IMAGE_VERSION(&compiled);linkedp = IMG_Linked_Version();
        printf("We compiled against SDL_image version %d.%d.%d ...\n",compiled.major, compiled.minor, compiled.patch);
        printf("But we are linking against SDL_image version %d.%d.%d.\n",linkedp->major, linkedp->minor, linkedp->patch);
    }
    //Engines
    Engine::Start<RenderEngine>(REOptions());
    Engine::Start<InputEngine>(IEOptions());
    if(Settings::getAs<bool>(Settings::MUSIC_ON)){
        Engine::Start<SoundEngine>(SEOptions());
    }
    Engine::Start<FileEngine>(FEOptions());
    Engine::Start<LuaEngine>(LEOptions());
    //Start resources
    if(!Resources::Initialize()){
        Logger::write();
        return 1;
    }
    //Initialize font
    Resources::AddResource(new Resource(
            &Init_FONT,
            [](void* res){TTF_CloseFont(static_cast<TTF_Font*>(res));},
            [](void* res){return static_cast<TTF_Font*>(res) != NULL;}
        ),
        Resources::FONT,true
    );
    //Initialize vector of textures 
    Resources::AddResource(new Resource(
            &Init_TEXTURES,
            [](void* res){for(Texture* t : *static_cast<vector<Texture*>*>(res)){if(t!=NULL){delete t;}}},
            [](void* res){return static_cast<vector<Texture*>*>(res) != NULL;}
        ),
        Resources::TEXTURES,true
    );
    //Init graph
    SceneGraph game;
    //Add node
    game.Add(new SpaceGame(&game));
    //Start Game
    game.Loop(0);
    //Stop engines
    Engine::Stop<LuaEngine>(LEOptions());
    Engine::Stop<FileEngine>(FEOptions());
    Engine::Stop<SoundEngine>(SEOptions());
    Engine::Stop<InputEngine>(IEOptions());
    //Engine::Stop<RenderEngine>(REOptions());
    //Free resources
    Resources::Dealloc();

    return 0;
}

//TODO header interface
//TODO no resource init
//TODO move input to engine
//TODO add override keywords