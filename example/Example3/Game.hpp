#pragma once
#include <vector>

//Include default systems and components
#include "ecs/ECS.hpp"

//Include scene class and logger
#include "utils/Scene.hpp"
#include "utils/Logger.hpp"

using namespace std;

//Texture resource initialization
void* Init_TEXTURES(){
    try{
        vector<Texture*>* textures = new vector<Texture*>(Resources::TEXTURES_TOTAL);
        fs::path temp_player = Settings::getAs<fs::path>(Settings::MEDIA_PATH)/Settings::getAs<fs::path>(Settings::TEXTURE_PLAYER);
        (*textures)[Resources::PLAYER_TEXTURE] = new Texture();
        (*textures)[Resources::PLAYER_TEXTURE]->load(temp_player.string(),RenderEngine::getRenderer());
        return textures;
    }catch(const exception&){//TODO Error message
        return NULL;
    }
}
//Font resource initialization
void* Init_FONT(){
    fs::path temp = Settings::getAs<fs::path>(Settings::MEDIA_PATH)/Settings::getAs<fs::path>(Settings::FONT);
    string fontPath(temp.string());//Convert to string
    return TTF_OpenFont(fontPath.c_str(),20);
        //Logger::logERROR("Failed to load font from :"+fontPath+" ,SDL_ttf Error: "+string(TTF_GetError()));
}

//Empty game, minimal example
class ExampleGame : public Scene{
    public:
    ExampleGame(SceneGraph* _sg):
        Scene(_sg,0,new vector<pair<size_t,Resource*>>({
            {
                Resources::TEXTURES,
                new Resource(
                    &Init_TEXTURES,
                    [](void* res){for(Texture* t : *static_cast<vector<Texture*>*>(res)){if(t!=NULL){delete t;}}},
                    [](void* res){return static_cast<vector<Texture*>*>(res) != NULL;}
                )
            },
            {
                Resources::FONT,
                new Resource(
                    &Init_FONT,
                    [](void* res){TTF_CloseFont(static_cast<TTF_Font*>(res));},
                    [](void* res){return static_cast<TTF_Font*>(res) != NULL;}
                )
            }
        }))
    {
        //Trick to initialize resources here
        Init();

        //Create Component storages
        manager.createComponentStore<PhysicsComponent>();
        manager.createComponentStore<TypeComponent>();
        manager.createComponentStore<SpriteComponent>();
        manager.createComponentStore<ConsoleComponent>();
        manager.createComponentStore<InputComponent>();

        //Commands for console
        consoleFunctions = {
            {"teleportPlayer",{[this](vector<string>& par){teleportPlayer(stof(par[0]),stof(par[1]));},2}},
            {"displayPosition",{[this](vector<string>& par){writePlayerPos();},0}}
        };

        //Create Systems
        manager.addSystem(System::SysPtr(new PhysicsSystem(manager)));
        manager.addSystem(System::SysPtr(new RenderSystem(manager,RenderEngine::getRenderer(),textures,&cam)));
        manager.addSystem(System::SysPtr(new ConsoleSystem(manager,&events,RenderEngine::getFont(),RenderEngine::getRenderer(),_sg,&consoleFunctions)));

        //Create Console
        createConsole();
        
        //Create Box2D world
        world = new b2World({0.0f,0.0f});
        world->SetAllowSleeping(true);
        world->SetWarmStarting(true);
        
        //Create player
        player = manager.createEntity();
        float_2D size = {24.0f,24.0f};
        float_2D pos = {1000.0f,1000.0f};
        manager.addComponent(player,PhysicsComponent(world,pos,size,true,true,{0.0f,1.0f,0.1f,0.0f,15.0f}));
        manager.addComponent(player,SpriteComponent(RenderEngine::getPlayerTexture(),{10,3,13,26}));
        manager.registerEntity(player);
    }

    //From Scene
    void PostInit() override{
        reloadSettings();
    }
    //Reload settings function, called after settings change
    void reloadSettings() override{
        //Camera size
        cam.size = {Settings::getAs<int32_t>(Settings::SCREEN_WIDTH),Settings::getAs<int32_t>(Settings::SCREEN_HEIGHT)};

        //Unset dirty bit
        Settings::unsetDirty();
    }
    void Update(const float dt) override{
        manager.update(dt);
        world->Step(1/60.0f,6,3);
        updateCamPos(dt,player,cam);
    }
    void DeInit() override{}

    vector<Texture*>* textures;

    //Camera:
    Camera cam;

    //Player
    Entity player;

    //Box2D World
    b2World* world;

    //Update cam position to center on player
     void updateCamPos([[maybe_unused]] const float dt, Entity player, Camera& cam){
        PhysicsComponent col = manager.getComponent<PhysicsComponent>(player);
        b2Vec2 pos = col.body->GetPosition();
        int_2D center = {(int)(pos.x*PPM),(int)(pos.y*PPM)};
        cam.pos = {center.x-(int)(cam.size.x/2.0f),center.y-(int)(cam.size.y/2.0f)};
    }

    
    //Console
    Entity console;
    map<string,pair<function<void(vector<string>&)>,size_t>> consoleFunctions;
    void createConsole(){
        console = manager.createEntity();
        manager.addComponent(console,InputComponent(false));
        manager.addComponent(console,ConsoleComponent(""));
        manager.registerEntity(console);
    }

    //Scene Console commands
    void teleportPlayer(float x, float y){
        PhysicsComponent& p = manager.getComponent<PhysicsComponent>(player);
        p.setPos({x,y});
    }
    void writePlayerPos(){
        PhysicsComponent& p = manager.getComponent<PhysicsComponent>(player);
        Logger::logINFO(to_string(p.getPos().x)+" "+to_string(p.getPos().y));
    }
};