#include <stdio.h>
#include <string>

#include "Game.hpp"
#include "SceneGraph.hpp"
#include "utils/Settings.hpp"
#include "utils/Resources.hpp"

using namespace std;
//Texture resource initialization
void* Init_TEXTURES(){
    try{
        //Resource will be a pointer to a vector of pointers to textures
        vector<Texture*>* textures = new vector<Texture*>(Resources::TEXTURES_TOTAL);
        //Allocated player texture
        fs::path temp_player = Settings::getAs<fs::path>(Settings::MEDIA_PATH)/Settings::getAs<fs::path>(Settings::TEXTURE_PLAYER);
        (*textures)[Resources::PLAYER_TEXTURE] = new Texture(); //Empty texture
        (*textures)[Resources::PLAYER_TEXTURE]->load(temp_player.string(),RenderEngine::getRenderer()); //Load from assets
        return textures;
    }catch(const exception&){//TODO Error message
        return NULL;
    }
}

int main(int argc, char** argv){
    //Apply settings, mandatory variables
    Settings::set(Settings::FPS, "60");
    Settings::set(Settings::SCREEN_WIDTH,"800");
    Settings::set(Settings::SCREEN_HEIGHT, "600");
    Settings::set(Settings::MEDIA_PATH, "data");
    Settings::set(Settings::TEXTURE_PLAYER, "player.png"); //Player texture
    Settings::set(Settings::DEBUG_LEVEL, "0");

    //Engines
    Engine::Start<RenderEngine>(REOptions());

    //Start resources
    if(!Resources::Initialize()){
        Logger::write();
        return 1;
    }
    //Initialize vector of textures 
    Resources::AddResource(new Resource(
            &Init_TEXTURES, //Resource init function
            [](void* res){for(Texture* t : *static_cast<vector<Texture*>*>(res)){if(t!=NULL){delete t;}}}, //Deallocation function
            [](void* res){return static_cast<vector<Texture*>*>(res) != NULL;} //Function for checking if allocated
        ),
        Resources::TEXTURES,true //Resource ID and if it should be allocated immediatelly
    );
    //Create a scene graph
    SceneGraph graph;

    //Add node (scene)
    graph.Add(new ExampleGame(&graph));

    //Start Game (main loop), 0 is SceneID of ExampleGame - entry point
    graph.Loop(0);

    //Free resources
    Resources::Dealloc();

    return 0;
}