#pragma once
#include <vector>

//Include default systems and components
#include "ecs/ECS.hpp"

//Include scene class and logger
#include "utils/Scene.hpp"
#include "utils/Logger.hpp"

using namespace std;

//Empty game, minimal example
class ExampleGame : public Scene{
    public:
    ExampleGame(SceneGraph* _sg):
        Scene(_sg,0,new vector<pair<size_t,Resource*>>({}))
    {
        //Create Component storages
        manager.createComponentStore<PhysicsComponent>();
        manager.createComponentStore<TypeComponent>();
        manager.createComponentStore<SpriteComponent>();

        //Create Systems
        manager.addSystem(System::SysPtr(new PhysicsSystem(manager)));
        manager.addSystem(System::SysPtr(new RenderSystem(manager,RenderEngine::getRenderer(),textures,&cam)));
        
        //Create Box2D world
        world = new b2World({0.0f,0.0f});
        world->SetAllowSleeping(true);//For better performance
        world->SetWarmStarting(true);//For better performance
        
        //Create player
        player = manager.createEntity();
        float_2D size = {24.0f,24.0f};
        float_2D pos = {1000.0f,1000.0f};
        manager.addComponent(player,PhysicsComponent(world,pos,size,true,true,{0.0f,1.0f,0.1f,0.0f,15.0f}));
        manager.addComponent(player,SpriteComponent(RenderEngine::getPlayerTexture(),{10,3,13,26}));
        manager.registerEntity(player);
    }

    //From Scene
    void PostInit() override{
        reloadSettings();
    }
    //Reload settings function, called after settings change
    void reloadSettings() override{
        //Camera size
        cam.size = {Settings::getAs<int32_t>(Settings::SCREEN_WIDTH),Settings::getAs<int32_t>(Settings::SCREEN_HEIGHT)};

        //Unset dirty bit
        Settings::unsetDirty();
    }
    void Update(const float dt) override{
        manager.update(dt);
        world->Step(1/60.0f,6,3);
        updateCamPos(dt,player,cam);
    }
    void DeInit() override{}

    vector<Texture*>* textures;

    //Camera:
    Camera cam;

    //Player
    Entity player;

    //Box2D World
    b2World* world;

    //Update cam position to center on player
     void updateCamPos([[maybe_unused]] const float dt, Entity player, Camera& cam){
        //Accessing a component
        PhysicsComponent col = manager.getComponent<PhysicsComponent>(player);
        b2Vec2 pos = col.body->GetPosition();//Accessing position of players physic body
        int_2D center = {(int)(pos.x*PPM),(int)(pos.y*PPM)};//Scaling from Box2D coordinates
        cam.pos = {center.x-(int)(cam.size.x/2.0f),center.y-(int)(cam.size.y/2.0f)};
    }
};