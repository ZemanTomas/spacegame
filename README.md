## SpaceGame Framework

A 2D game framework built on libSDL using ECS architecture

### Cloning this repository:
 - git clone http://gitlab.com/ZemanTomas/SpaceGame
 - git submodule update --init --recursive

### Building (Linux):
- If not present, install GNU Make and CMake: sudo apt-get install make cmake
- Building is provided using CMake primarily (V3.20+ recommended)
- Examples provide an example project using this framework including a simple CMakeLists

### Building (Windows):
- Install g++-mingw-w64-x86-64  (at least version 9.2, versions < C++23 untested)

### Building examples:
 - See example/README.md

### Using:
##### From SDL:
- Accelerated 2D render API
- User input
- Application and window state changes
- Audio
- File I/O

##### ECS implementation:
- This project adapts the following ECS implementation : https://github.com/SRombauts/ecs

### Issues:
ambigous byte : https://stackoverflow.com/questions/55601740/how-to-crosscompile-applications-on-ubuntu-to-windows

### TODO: (TODO.sh prints all //TODO comments)
- Code rewrite
- Performance reporting
- More targets for CMake
- Multithreading, coroutines etc...
- Test C++20 and lower
- Packaging,Installer
- Better GUI library
- Better asset management, tilemaps...
- Camera component/rendering
- Camera zoom support
- More robust ECS
- Deprecate int_2D, float_2D

### Media Credits:
- https://opengameart.org/content/dungeon-crawl-32x32-tiles
- https://opengameart.org/content/menu-music
- https://opengameart.org/content/playerunit-sheet
- https://www.1001freefonts.com/8-bit-operator.font

### Libraries (in case of manual dependency initialization):
 - Put these libraries in their subfolders:
 - https://github.com/libsdl-org/SDL in libs/SDL2
 - https://github.com/libsdl-org/SDL_ttf in libs/SDL2_ttf
 - https://github.com/libsdl-org/SDL_image in libs/SDL2_image
 - https://github.com/libsdl-org/SDL_mixer in libs/SDL2_mixer
 - https://github.com/mattreecebentley/plf_nanotimer.git in libs/plf_nanotimer
 - https://github.com/erincatto/box2d.git in libs/box2d
 - https://github.com/ocornut/imgui.git in libs/imgui
 - https://github.com/epezent/implot in libs/implot
 - https://github.com/torch/luajit-rocks in libs/luajit
 - https://github.com/AdUki/LuaState.git in libs/LuaState