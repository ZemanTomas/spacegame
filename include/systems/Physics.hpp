#pragma once
#include "ecs/Manager.hpp"
#include "components/Physics.hpp"
using namespace std;

class World;

//Physics System
//Requires PhysicsComponent
class PhysicsSystem : public System{
    public:
    PhysicsSystem(Manager& _m):System(_m){
        setRequiredComponents({PhysicsComponent::ctype});
    }
    //Main loop (for each entity) - handles simple physics, currently unused
    virtual void updateEntity(__attribute__((unused)) float dt,__attribute__((unused))  Entity _e){
        __attribute__((unused)) PhysicsComponent& p = manager.getComponentStore<PhysicsComponent>().get(_e);
        
        switch(p.ptype){
            case PhysicsComponent::Type::Solid:
                //
                break;
            case PhysicsComponent::Type::Dynamic:
                //

                //Move
                p.pos+=p.vel;
                break;
            case PhysicsComponent::Type::Solid_B2D:
                //Handled by B2D world update
                break;
            case PhysicsComponent::Type::Dynamic_B2D:
                //Handled by B2D world update
                break;
        }
    }
    private:
};