#pragma once
//Mingw fix
#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif

#include <SDL.h>
#include "ecs/Manager.hpp"
#include "components/Input.hpp"
#include "components/Type.hpp"
#include "components/Physics.hpp"
#include "utils/Camera.hpp"

using namespace std;
//TODO: move into userspace

//Input System
//Requires InputComponent,TypeComponent
class PlayerInputSystem : public System{
    public:
    //Constructor, pointer to vector of events (typically from scene), camera centered on player (for rotation)
    PlayerInputSystem(Manager& _m, vector<SDL_Event>* _v,Camera* _c):System(_m),events(_v),cam(_c){
        setRequiredComponents({InputComponent::ctype,TypeComponent::ctype,PhysicsComponent::ctype});
    }
    //Main loop (for each entity) - gets input from queue and applies force to player
    virtual void updateEntity(__attribute__((unused)) float dt, Entity _e){
        InputComponent& in = manager.getComponentStore<InputComponent>().get(_e);
        const TypeComponent& type = manager.getComponentStore<TypeComponent>().get(_e);
        PhysicsComponent& body = manager.getComponentStore<PhysicsComponent>().get(_e);
        if(type.t == 0 && in.focus){//type.t == 0 -> player
            //Movement
            if(forceCont[0]){body.body->ApplyForce({0.0f,-30.0f},body.body->GetWorldCenter(),true);}
            if(forceCont[1]){body.body->ApplyForce({0.0f,30.0f},body.body->GetWorldCenter(),true);}
            if(forceCont[2]){body.body->ApplyForce({-30.0f,0.0f},body.body->GetWorldCenter(),true);}
            if(forceCont[3]){body.body->ApplyForce({30.0f,0.0f},body.body->GetWorldCenter(),true);}
            for(SDL_Event e : *events){
                if(e.type == SDL_KEYDOWN){
                    switch(e.key.keysym.sym){
                        case SDLK_w:
                        case SDLK_UP:   forceCont[0]=true;break;
                        case SDLK_s:
                        case SDLK_DOWN: forceCont[1]=true;break;
                        case SDLK_a:
                        case SDLK_LEFT: forceCont[2]=true;break;
                        case SDLK_d:
                        case SDLK_RIGHT:forceCont[3]=true;break;
                    }
                }else if(e.type == SDL_KEYUP){
                    switch(e.key.keysym.sym){
                        case SDLK_w:
                        case SDLK_UP:   forceCont[0]=false;break;
                        case SDLK_s:
                        case SDLK_DOWN: forceCont[1]=false;break;
                        case SDLK_a:
                        case SDLK_LEFT: forceCont[2]=false;break;
                        case SDLK_d:
                        case SDLK_RIGHT:forceCont[3]=false;break;
                    }
                }

            }
            b2Vec2 vel = body.body->GetLinearVelocity();
            float speed = vel.Normalize();
            //Speed limit
            if(speed > body.maxSpeed){body.body->SetLinearVelocity(body.maxSpeed * vel);}
            //Rotation
            int mouseX, mouseY;
            [[maybe_unused]] const int buttons = SDL_GetMouseState(&mouseX,&mouseY);
            float_2D mouse_pos = {static_cast<float>(mouseX), static_cast<float>(mouseY)};
            
            float_2D diff = (mouse_pos) - int_2D::toFloat_2D(cam->size)/2.0f;
            float inRads = atan2f(-diff.x,diff.y);
            body.body->SetTransform(body.body->GetPosition(),inRads+0.5f*M_PI);
		    //buttons & SDL_BUTTON(SDL_BUTTON_LEFT);
		    //buttons & SDL_BUTTON(SDL_BUTTON_RIGHT);
            //float_2D::toFloat_2D(cam->size/2)
        }
    }
    vector<SDL_Event>* events;
    bool forceCont[4] = {false,false,false,false}; //Force directions
    Camera* cam;
};