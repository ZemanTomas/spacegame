#pragma once
#include "ecs/Manager.hpp"
#include "components/GUI.hpp"
using namespace std;

class World;

//Physics System
//Requires GUIComponent
class GUISystem : public System{
    public:
    GUISystem(Manager& _m):System(_m){
        setRequiredComponents({GUIComponent::ctype});
    }
    //Main loop (for each entity) - simply runs function doGui for each GUIComponent
    virtual void updateEntity(__attribute__((unused)) float dt,__attribute__((unused))  Entity _e){
        __attribute__((unused)) GUIComponent& g = manager.getComponentStore<GUIComponent>().get(_e);
        g.values = g.doGui(g.name,g.params);
    }
};