#pragma once
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <functional>

#include <SDL.h>
#include <SDL_ttf.h>

#include "utils/Utils.hpp"
#include "utils/Texture.hpp"
#include "utils/Camera.hpp"
#include "utils/Settings.hpp"
#include "utils/Logger.hpp"
#include "components/Input.hpp"
#include "components/Console.hpp"
#include "engines/RenderEngine.hpp"
#include "SceneGraph.hpp"
//ECS
#include "ecs/Manager.hpp"

using namespace std;

//Commands class
struct Cmd{
    //Cmd can be one of OP types
    enum OP : uint8_t {
        SETSCENE,
        QUIT,
        SET_MUSIC,
        SET_DEBUGLEVEL,
        SET_FPS,
        SET_VOLUME,
        UNKNOWN,
        EMPTY
    } Op;
    //vector of parameters
    vector<string> params;
    //Static mapping of names to enums
    static map<string,OP> names;
};
map<string,Cmd::OP> Cmd::names = {
        {"setScene",SETSCENE},
        {"quit",QUIT},
        {"setMusic",SET_MUSIC},
        {"setDebugLevel",SET_DEBUGLEVEL},
        {"setFPS",SET_FPS},
        {"setFps",SET_FPS},
        {"setVolume",SET_VOLUME}
};

//Console System
//Requires InputComponent,ConsoleComponent
class ConsoleSystem : public System{
    public:
    //Constructor, pointer to vector of events, font, renderer, scenegraph, custom functions
    ConsoleSystem(Manager& _m,vector<SDL_Event>* _v,TTF_Font* _f,SDL_Renderer* _r,SceneGraph* _sg,map<string,pair<function<void(vector<string>&)>,size_t>> *_sf):
        System(_m),
        renderer(_r),
        events(_v),
        consoleOpen(false),
        promptText("/"),
        font(_f),
        sg(_sg),
        sceneFunctions(_sf)
    {
        setRequiredComponents({InputComponent::ctype,ConsoleComponent::ctype});

        //Initialize font if none
        if(font != NULL){
            promptTexture.loadText(&promptText,textColor,font,renderer);
        }
    }
    //Main loop (for each entity) - reads events and looks for a semicolon to turn on console, then handles keyboard input and executes commands
    virtual void updateEntity(__attribute__((unused)) float dt, __attribute__((unused)) Entity _e){
        //const ConsoleComponent& con = manager.getComponentStore<ConsoleComponent>().get(_e);
        //InputComponent& in = manager.getComponentStore<InputComponent>().get(_e);
        for(SDL_Event e : *events){
            if(e.key.keysym.sym == SDLK_SEMICOLON){//Semicolon -> toggle console
                if(e.type == SDL_KEYUP){continue;}//Prevent double-toggle
                if(consoleOpen){
                    consoleOpen = false;
                    SDL_StopTextInput();
                }else{
                    consoleOpen = true;
                    SDL_StartTextInput();
                }
            }else if(consoleOpen && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_BACKSPACE && promptText.length() > 1){//Backspace
                promptText.pop_back();
                promptTexture.loadText(&promptText,textColor,font,renderer);
            }else if(consoleOpen && e.type == SDL_TEXTINPUT){//Normal text
                promptText += e.text.text;
                promptTexture.loadText(&promptText,textColor,font,renderer);
            }else if(consoleOpen && e.key.keysym.sym == SDLK_RETURN && e.type == SDL_KEYDOWN){//Enter -> execute command
                auto command = consoleParse(promptText);
                Logger::logINFO(promptText);
                ConsoleSystem::callbacks[command.Op](this,command.params);
                //Reset commandline
                promptText = "/";
                promptTexture.loadText(&promptText,textColor,font,renderer);
                consoleOpen = false;
            }
        }
        if(consoleOpen && font != NULL && renderer != NULL && promptTexture.getTexture() != NULL){//Render console
            RenderEngine::RenderTexture(2,2,&promptTexture);
        }
    }
    private:
    SDL_Renderer* renderer;
    vector<SDL_Event>* events;
    bool consoleOpen;
    string promptText;
    Texture promptTexture;
    TTF_Font *font;
    SDL_Color textColor = {0,0,0,0xFF};
    SceneGraph* sg;
    map<string,pair<function<void(vector<string>&)>,size_t>> *sceneFunctions;//TODO function for adding
    //string command execution
    inline Cmd consoleParse(string cmd){//TODO direct exec
        Cmd ret;ret.Op=Cmd::EMPTY;
        if(cmd.size() <= 0){return ret;}
        if(cmd[0] == '/'){cmd.erase(0,1);}
        //tokenize
        vector<string> tokens = splitString(cmd);
        if(tokens.size() <= 0){return ret;}

        //If found, execute, if not, execute "unknown" cmd
        if(Cmd::names.contains(tokens[0])){
            return {Cmd::names[tokens[0]],vector<string>(tokens.cbegin()+1,tokens.cend())};
        }else{
            return {Cmd::UNKNOWN,vector<string>(tokens.cbegin(),tokens.cend())};
        }
    }

    //Commands
    static void setScene(ConsoleSystem* cs,vector<string>& par){
        if(par.size() != 1){return;}//ERROR
        try{
            cs->sg->Change(stoi(par[0]));
        }catch(const exception&){
            Logger::logERROR("Error while executing!");
        }
    }
    static void quitCommand(ConsoleSystem* cs,vector<string>& par){
        if(par.size() != 0){return;}//ERROR
        try{
            cs->sg->Quit();
        }catch(const exception&){
            Logger::logERROR("Error while executing!");
        }
    }
    static void setMusic([[maybe_unused]] ConsoleSystem* cs,vector<string>& par){
        if(par.size() != 1){return;}//ERROR
        try{
            Settings::set(Settings::MUSIC_ON,par[0]);
        }catch(const exception&){
            Logger::logERROR("Error while executing!");
        }
    }
    static void setDebugLevel([[maybe_unused]] ConsoleSystem* cs,vector<string>& par){
        if(par.size() != 1){return;}//ERROR
        try{
            Settings::set(Settings::DEBUG_LEVEL,par[0]);
        }catch(const exception&){
            Logger::logERROR("Error while executing!");
        }
    }
    static void setFPS([[maybe_unused]] ConsoleSystem* cs,vector<string>& par){
        if(par.size() != 1){return;}//ERROR
        try{
            Settings::set(Settings::FPS,par[0]);
        }catch(const exception&){
            Logger::logERROR("Error while executing!");
        }
    }
    static void setVolume([[maybe_unused]] ConsoleSystem* cs,vector<string>& par){
        if(par.size() != 1){return;}//ERROR
        try{
            Settings::set(Settings::VOLUME,par[0]);
        }catch(const exception&){
            Logger::logERROR("Error while executing!");
        }
    }
    //CustomFunctions
    static void unknownCommand([[maybe_unused]] ConsoleSystem* cs,[[maybe_unused]] vector<string>& par){
        string name = "";
        if(par.size()>0){
            name = par[0];
            //TODO Try Graph, Try Scene
            if(tryExecuteGraph(cs,name,vector<string>(par.cbegin()+1,par.cend())) || tryExecuteScene(cs,name,vector<string>(par.cbegin()+1,par.cend()))){//Try to find commands in scene or graph console functions
                return;
            }
        }
        Logger::logERROR("Unknown command: "+name);
    }
    //Try to execute string command from scenegraph console commands
    static bool tryExecuteGraph(ConsoleSystem* cs, string name, vector<string> par){
        auto it = cs->sg->consoleFunctions.find(name);
        if(it != cs->sg->consoleFunctions.end()){
            if(par.size() <= it->second.second){
                try{
                    it->second.first(par);
                    return true;
                }catch(const exception&){
                    Logger::logERROR("Error while executing!");
                }
            }else{
                Logger::logERROR("Not enough parameters!");
            }
        }
        return false;
    }
    //Try to execute string command from scene console commands
    static bool tryExecuteScene(ConsoleSystem* cs, string name, vector<string> par){
        if(cs->sceneFunctions){
            auto it = cs->sceneFunctions->find(name);
            if(it != cs->sceneFunctions->end()){
                if(par.size() <= it->second.second){
                    try{
                        it->second.first(par);
                        return true;
                    }catch(const exception&){
                        Logger::logERROR("Error while executing!");
                    }
                }else{
                    Logger::logERROR("Not enough parameters!");
                }
            }
        }
        return false;
    }
    static void emptyCommand([[maybe_unused]] ConsoleSystem* cs,[[maybe_unused]] vector<string>& par){}
    static map<Cmd::OP,void (*)(ConsoleSystem*,vector<string>&)> callbacks;
};
//Mapping CMD enum -> functions
map<Cmd::OP,void (*)(ConsoleSystem*,vector<string>&)> ConsoleSystem::callbacks = {//TODO merge with cmd map
    {Cmd::SETSCENE,&ConsoleSystem::setScene},
    {Cmd::QUIT,&ConsoleSystem::quitCommand},
    {Cmd::SET_MUSIC,&ConsoleSystem::setMusic},
    {Cmd::SET_DEBUGLEVEL,&ConsoleSystem::setDebugLevel},
    {Cmd::SET_FPS,&ConsoleSystem::setFPS},
    {Cmd::SET_VOLUME,&ConsoleSystem::setVolume},
    //
    {Cmd::EMPTY,&ConsoleSystem::emptyCommand},
    {Cmd::UNKNOWN,&ConsoleSystem::unknownCommand}
};