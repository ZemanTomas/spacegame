#pragma once
#include <SDL_mixer.h>

#include "ecs/Manager.hpp"

#include "components/Sound.hpp"
#include "utils/Logger.hpp"
#include "engines/SoundEngine.hpp"
using namespace std;
//Audio System
//Requires SoundComponent
class AudioSystem : public System{
    public:
    AudioSystem(Manager& _m):System(_m){
        setRequiredComponents({SoundComponent::ctype});
    }
    //Main loop (for each entity) - updates state of music according to component
    virtual void updateEntity(__attribute__((unused)) float dt, Entity _e){
        if(!Engine::getInstance<SoundEngine>().Inited()){return;}
        SoundComponent& snd = manager.getComponentStore<SoundComponent>().get(_e);
        //Process music part
        if(snd.music != NULL){
            if((snd.action & MUSIC_PLAY) != 0){
                if(Mix_PlayingMusic() == 0){Mix_PlayMusic(snd.music,-1);}
                if(Mix_PausedMusic() == 1){Mix_ResumeMusic();}
            }else if((snd.action & MUSIC_PAUSE) != 0){
                if(Mix_PlayingMusic() == 1){Mix_PauseMusic();}
            }else{
                Mix_HaltMusic();
            }
        }
        //Process chunk part and reset action
        if(snd.chunk != NULL){
            if((snd.action & CHUNK_PLAY) != 0){
                Mix_PlayChannel(-1,snd.chunk,0);
                snd.action ^= CHUNK_PLAY;
            }
            if((snd.action & CHUNK_PLAY_TIMES) != 0){
                Mix_PlayChannel(-1,snd.chunk,snd.param);
                snd.action ^= CHUNK_PLAY_TIMES;
            }
            if((snd.action & CHUNK_PLAY_INFINITE) != 0){
                Mix_PlayChannel(-1,snd.chunk,-1);
                snd.action ^= CHUNK_PLAY_INFINITE;
            }
        }
    }
};