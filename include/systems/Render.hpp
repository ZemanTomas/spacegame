#pragma once
#include <SDL.h>
#include "ecs/Manager.hpp"
#include "components/Physics.hpp"
#include "components/Sprite.hpp"
#include "utils/Texture.hpp"
#include "utils/Camera.hpp"
#include "utils/Resources.hpp"
#include "engines/RenderEngine.hpp"

using namespace std;

//Render System
//Requires SpriteComponent
class RenderSystem : public System{//TODO move renderer to render engine
    public:
    //Constructor, holds vector of textures for easy access and camera for rendering
    RenderSystem(Manager& _m,SDL_Renderer* _r,vector<Texture*>* _tex,Camera* _c):System(_m),renderer(_r),textures(_tex),cam(_c){
        setRequiredComponents({SpriteComponent::ctype});
    }
    //Main loop (for each entity) - renders each spriteComponent
    virtual void updateEntity(__attribute__((unused)) float dt, Entity _e){
        SpriteComponent& sprite = manager.getComponentStore<SpriteComponent>().get(_e);
        //Render bound to body
        SDL_Rect dst,src = sprite.getCurrentSrc(++(sprite.currenttick));
        if(sprite.body){
            const PhysicsComponent& body = manager.getComponentStore<PhysicsComponent>().get(_e);

            if(sprite.texture){//If sprite has a texture
                RenderEngine::RenderBody(sprite.texture->getTexture(),body.body,cam,src,sprite.z);
            }else{//compute destination rectangle
                const PhysicsComponent::Collider& col_f = body.collider;

                const float_2D& pos_f = body.pos;
                int_2D pos = {(int)pos_f.x,(int)pos_f.y};
                int_2D size = {(int)col_f.size.x,(int)col_f.size.y};
                if(!cam->visible(pos,size)){return;}

                int_2D dstpos = cam->getRelativePos({pos.x-size.x/2,pos.y-size.y/2});

                dst = {dstpos.x,dstpos.y,size.x,size.y};
                RenderEngine::RenderColour(sprite.colour,dst,sprite.z);
            }
        }else{
            if(sprite.texture){//If sprite has a texture
                RenderEngine::RenderTexture(sprite.dst.x,sprite.dst.y,sprite.texture,&src,{sprite.dst.w,sprite.dst.h},cam,sprite.z,sprite.angle);
            }else{
                RenderEngine::RenderColour(sprite.colour,sprite.dst,sprite.z);
            }
        }
    }
    private:
    SDL_Renderer* renderer;
    vector<Texture*>* textures;
    Camera* cam;
};