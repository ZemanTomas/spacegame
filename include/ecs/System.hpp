#pragma once
#include <memory>
#include <set>
#include <unordered_set>

#include "ecs/Component.hpp"
#include "ecs/Entity.hpp"

class Manager;//Forward declaration

//System parent class
class System{
    public:
    typedef shared_ptr<System> SysPtr;
    explicit System(Manager& _m):manager(_m){}
    virtual ~System(){}
    inline const std::set<ComponentType>& getReqComp() const {return requiredComponents;}
    inline bool hasComponents(const std::set<ComponentType>& entityCom) const {
        return std::includes(entityCom.begin(),entityCom.end(),requiredComponents.begin(),requiredComponents.end());
    }
    inline bool addEntity(Entity _e){return matchingEntities.insert(_e).second;}
    inline std::size_t removeEntity(Entity _e){return matchingEntities.erase(_e);}
    inline bool hasEntity(Entity _e) const {return matchingEntities.find(_e) != matchingEntities.end();}
    //Update loop, loops over all entities registered to this system
    std::size_t update(float dt){
        std::size_t ret = 0;
        for(auto entity = matchingEntities.begin(); entity!=matchingEntities.end(); ++entity){
            updateEntity(dt,*entity);
            ++ret;
        }
        return ret;
    }
    virtual void updateEntity(float dt, Entity _e) = 0;

    protected:
    inline void setRequiredComponents(std::set<ComponentType>&& _reqset){requiredComponents = std::move(_reqset);}
    Manager& manager;

    private:
    std::set<ComponentType> requiredComponents;
    std::unordered_set<Entity> matchingEntities;
};