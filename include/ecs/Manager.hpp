#pragma once
#include <unordered_map>
#include <map>
#include <vector>
#include <algorithm>

#include "ecs/Entity.hpp"
#include "ecs/Component.hpp"
#include "ecs/System.hpp"
#include "ecs/ComponentStore.hpp"

using namespace std;

//ECS manager class
class Manager{
    public:
    Manager():lastEntity(INVALID_ENTITY),entities(),componentStores(),systems(){}
    ~Manager(){
        for(auto& c_ptr : componentStores){
            c_ptr.second.get()->_removeAll();
        }
    }
    //Create component storage
    template<typename T> inline bool createComponentStore(){
        //Asserts
        return componentStores.insert(make_pair(T::ctype,IComponentStore::ComPtr(new ComponentStore<T>()))).second;
    }
    //Return component storage
    template<typename T> inline ComponentStore<T>* getComponentStorePtr(){
        auto iComponentStore = componentStores.find(T::ctype);
        if (componentStores.end() == iComponentStore) {throw std::runtime_error("The ComponentStore does not exist");}
        return reinterpret_cast<ComponentStore<T>*>(iComponentStore->second.get());
    }
    template<typename T> inline ComponentStore<T>& getComponentStore(){
        return *(getComponentStorePtr<T>());
    }
    //Return component
    template<typename T> inline T& getComponent(const Entity _e){
        return getComponentStore<T>().get(_e);
    }
    //Adds system to manager
    void addSystem(const System::SysPtr& _sptr){
        //if((!_sptr)||(_sptr->getReqComp().empty())){/*ERROR*/}
        systems.push_back(_sptr);
    }
    //Creates a new entity 
    Entity inline createEntity(){entities.insert(make_pair(lastEntity+1,set<ComponentType>()));return ++lastEntity;}
    //Adds component to entity
    template<typename T> inline bool addComponent(const Entity _e,T&& _c){
        auto entity = entities.find(_e);
        if(entities.end() == entity){return false;}//ERROR
        entity->second.insert(T::ctype);
        return getComponentStore<T>().add(_e,move(_c));
    }
    //Registers entity and its components to manager and systems
    size_t registerEntity(const Entity _e){
        size_t ret = 0;//Number of associated systems
        auto entity = entities.find(_e);
        if(entities.end() == entity){return ret;}//ERROR

        //Find systems that need components of entity _E and add _e to them
        for(auto system = systems.begin(); system != systems.end(); ++system){//for(:)
            auto sysReqCom = (*system)->getReqComp();
            if((*system)->hasComponents(entity->second)){
                (*system)->addEntity(_e);
                ++ret;
            }
        }
        return ret;
    }
    //Unregisters entity from manager and systems
    size_t unregisterEntity(const Entity _e){
        size_t ret = 0;
        auto entity = entities.find(_e);
        if(entities.end() == entity){return ret;}
        for(auto system = systems.begin(); system != systems.end(); ++system){//for(:)
            ret += (*system)->removeEntity(_e);
        }
        
        for(auto comType : entity->second){
            componentStores.at(comType)->_remove(_e);
        }

        entities.erase(_e);
        return ret;
    }
    //Update loop, updates all systems
    size_t update(float dt){
        size_t ret = 0;
        for(auto system = systems.begin(); system != systems.end(); ++system){//for(:)
            ret += (*system)->update(dt);
        }
        return ret;
    }

    //private:
    Entity lastEntity;
    unordered_map<Entity,set<ComponentType>> entities; //TODO: multiset
    map<ComponentType, IComponentStore::ComPtr> componentStores;
    vector<System::SysPtr> systems;
};