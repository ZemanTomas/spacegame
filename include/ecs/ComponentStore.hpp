#pragma once
#include <unordered_map>
#include <memory>

#include "ecs/Entity.hpp"
#include "ecs/Component.hpp"

using namespace std;

class IComponentStore{
    public:
    typedef unique_ptr<IComponentStore> ComPtr;
    virtual bool _remove(Entity){return false;};
    virtual void _removeAll(){};
    virtual ~IComponentStore(){};
};
//Component storage
template <typename T> class ComponentStore : public IComponentStore{
    public:
    ComponentStore(){}
    ~ComponentStore(){}
    //Register component T to entity
    inline bool add(const Entity _e, T&& _c){return store.insert(make_pair(_e,move(_c))).second;}
    //Delete component T
    inline bool remove(Entity _e){return 0<store.erase(_e);}
    bool _remove(Entity _e) override {return 0<store.erase(_e);}//virtual, slower
    //Delete all components
    void _removeAll() override {return store.clear();}//virtual
    //Is T registered for entity
    inline bool has(Entity _e) const {return store.end() != store.find(_e);}
    //Get component T
    inline T& get(Entity _e){return store.at(_e);}
    //Get and erase component T
    inline T extract(Entity _e){T component = move(store.at(_e));store.erase(_e);return component;}
    //Returns component storage
    inline const unordered_map<Entity,T>& getComponents(){return store;}
    private:
    unordered_map<Entity,T> store;
    static const ComponentType ctype = T::ctype;
};