#pragma once
#include <stdint.h>

//Component definition

typedef uint32_t ComponentType;
static const ComponentType INVALID_COMPONENT_TYPE = 0;
struct Component{
    static const ComponentType ctype = INVALID_COMPONENT_TYPE;
};