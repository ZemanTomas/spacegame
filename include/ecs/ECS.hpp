#pragma once
using namespace std;

//Wrapper for including/enumerating all components/systems

//Components
#include "components/Physics.hpp"
#include "components/Type.hpp"
#include "components/Console.hpp"
#include "components/Input.hpp"
#include "components/Sound.hpp"
#include "components/Sprite.hpp"
#include "components/GUI.hpp"

//Systems
#include "systems/Console.hpp"
#include "systems/Render.hpp"
#include "systems/Physics.hpp"
#include "systems/PlayerInput.hpp"
#include "systems/Audio.hpp"
#include "systems/GUI.hpp"

//Enumeration of ComponentTypes

const ComponentType TypeComponent::ctype = __COUNTER__;
const ComponentType PhysicsComponent::ctype = __COUNTER__;
const ComponentType InputComponent::ctype = __COUNTER__;
const ComponentType ConsoleComponent::ctype = __COUNTER__;
const ComponentType SoundComponent::ctype = __COUNTER__;
const ComponentType SpriteComponent::ctype = __COUNTER__;
const ComponentType GUIComponent::ctype = __COUNTER__;