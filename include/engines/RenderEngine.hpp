#pragma once
#include <SDL.h>
#include "utils/Engine.hpp"
#include "utils/Texture.hpp"
#include "utils/Utils.hpp"

#include <stdio.h>
#include <queue>

using namespace std;

struct REOptions : Options{//TODO options: window name

};

//RenderEngine, provides functions to initialize resources for rendering/window
class RenderEngine : public Engine{
    public:
    //Virtual engine interface
    void Init(Options& _o){
        [[maybe_unused]] REOptions& o = static_cast<REOptions&>(_o);
        //Parse options
        //Initialize resources
        //Initialize SDL, resource is bool
        Resources::AddResource(new Resource(
                &Init_SDL,
                []([[maybe_unused]] void* res){SDL_Quit();(*static_cast<bool*>(res)) = false;},
                [](void* res){return res && (*static_cast<bool*>(res));}
            ),
            Resources::SDL,true
        );
        //Initialize window for rendering
        Resources::AddResource(new Resource(
                &Init_WINDOW,
                [](void* res){SDL_DestroyWindow(static_cast<SDL_Window*>(res));res = NULL;},
                [](void* res){return static_cast<SDL_Window*>(res) != NULL;}
            ),
            Resources::WINDOW,true
        );
        //Initialize renderer
        Resources::AddResource(new Resource(
                &Init_RENDERER,
                [](void* res){SDL_DestroyRenderer(static_cast<SDL_Renderer*>(res));res = NULL;},
                [](void* res){return static_cast<SDL_Renderer*>(res) != NULL;}
            ),
            Resources::RENDERER,true
        );
        //Initialize PNG loading, resource is bool
        Resources::AddResource(new Resource(
                &Init_PNG,
                []([[maybe_unused]] void* res){IMG_Quit();(*static_cast<bool*>(res)) = false;},
                [](void* res){return res && (*static_cast<bool*>(res));}
            ),
            Resources::PNG,true
        );
        //Initialize TTF rendering, resource is bool
        Resources::AddResource(new Resource(
                &Init_TTF,
                []([[maybe_unused]] void* res){TTF_Quit();(*static_cast<bool*>(res)) = false;},
                [](void* res){return res && (*static_cast<bool*>(res));}
            ),
            Resources::TTF,true
        );

    }
    void DeInit(Options& _o){
        [[maybe_unused]] REOptions& o = static_cast<REOptions&>(_o);
        for(auto& r : res_enums){
            Resources::Dealloc(r);
        }
    }
    void ReInit(Options& _o){//TODO reinit
        [[maybe_unused]] REOptions& o = static_cast<REOptions&>(_o);
    }
    bool Inited(){
        for(auto& r : res_enums){
            if(!Resources::Allocated(r)){return false;}
        }
        return true;
    }
    //User interface/Wrappers
    static SDL_Window* getWindow(){return static_cast<SDL_Window*>(Resources::getInstance()[Resources::WINDOW]);}
    static TTF_Font* getFont(){return static_cast<TTF_Font*>(Resources::getInstance()[Resources::FONT]);}
    static SDL_Renderer* getRenderer(){return static_cast<SDL_Renderer*>(Resources::getInstance()[Resources::RENDERER]);}
    static vector<Texture*>* getTextures(){return static_cast<vector<Texture*>*>(Resources::getInstance()[Resources::TEXTURES]);}

    static Texture* getPlayerTexture(){return (*getTextures())[Resources::PLAYER_TEXTURE];}
    static Texture* getTextureMap(){return (*getTextures())[Resources::MAP_TEXTURE];}

    
    //Renders texture at x:y(world space), src as source rect (from texture space), size (in pixels) overrides default texture size
    static void RenderTexture(int x, int y,Texture* texture,const SDL_Rect* src = NULL,const int_2D size = {0,0},Camera* cam = nullptr,uint8_t z = 0,float angle = 0.0f){
        SDL_Rect dstGlobal = {x, y, texture->getWidth(), texture->getHeight()};
        //If size given, override
        if(size.x != 0){dstGlobal.w = size.x;}
        if(size.y != 0){dstGlobal.h = size.y;}
        //Test visibility
        if(cam){
            SDL_Rect camRect = {cam->pos.x,cam->pos.y,cam->size.x+dstGlobal.w,cam->size.y+dstGlobal.h};
            if(!SDL_HasIntersection(&dstGlobal,&camRect)){return;}//Not visible
        }
        //Render
        const SDL_Rect dstLocal = {
            x - (cam ? cam->pos.x+(int)(PPM / 2.0f) : 0),
            y - (cam ? cam->pos.y+(int)(PPM / 2.0f) : 0),
            dstGlobal.w,
            dstGlobal.h
        };
        //SDL_RenderCopy(getRenderer(), texture->getTexture(), src, &dstLocal);
        getRenderQueue()->emplace(texture->getTexture(),src ? *src : SDL_Rect({0,0,0,0}),dstLocal,angle*57.2957795f,SDL_FLIP_NONE,0,z);
    }
    //Renders texture over box2D shape at pos, srcRect (from texture space) to rectangle if shape is a polygon, or minimum square if shape is a circle
    static void RenderShape(SDL_Texture* texture,const SDL_Rect& srcRect,const b2Shape* shape,b2Vec2 pos,float angle,uint8_t z = 0){
        b2Shape::Type type = shape->GetType();
        if(type == b2Shape::e_polygon){//Rectangle
            const b2PolygonShape* pshape = (b2PolygonShape*)(shape);
            float_2D size = {pshape->m_vertices[1].x-pshape->m_vertices[0].x,pshape->m_vertices[2].y-pshape->m_vertices[1].y};
            //Destination, scale by PPM
            const SDL_Rect dstRect = {
                (int)((pos.x-(size.x/2.0f))*PPM),
                (int)((pos.y-(size.y/2.0f))*PPM),
                (int)(size.x*PPM),
                (int)(size.y*PPM)
            };
            //SDL_RenderCopyEx(getRenderer(),texture,&srcRect,&dstRect,angle*57.2957795f,NULL,SDL_FLIP_NONE);
            getRenderQueue()->emplace(texture,srcRect,dstRect,angle*57.2957795f,SDL_FLIP_NONE,0,z);//TODO: angle constant global
        }else if(type == b2Shape::e_circle){//Circle
            const b2CircleShape* cshape = (b2CircleShape*)(shape);
            float size = cshape->m_radius*2.0f;
            //Destination, square of size radius, scale by PPM
            const SDL_Rect dstRect = {
                (int)((pos.x-(size/2.0f))*PPM),
                (int)((pos.y-(size/2.0f))*PPM),
                (int)(size*PPM),
                (int)(size*PPM)
            };
            //SDL_RenderCopyEx(getRenderer(),texture,&srcRect,&dstRect,angle*57.2957795f,NULL,SDL_FLIP_NONE);
            getRenderQueue()->emplace(texture,srcRect,dstRect,angle*57.2957795f,SDL_FLIP_NONE,0,z);//TODO: multiply angle from body with angle from component
        }
    }
    //Renders box2D body (global position) relative to cam, srcRect for selecting a part of a texture
    static void RenderBody(SDL_Texture* texture,b2Body* body,Camera* cam,const SDL_Rect& srcRect,uint8_t z){
        const b2Vec2 pos = body->GetPosition();
        const b2Fixture *flist = body->GetFixtureList();
        while(flist != NULL){//Render all fixtures(shapes)
            //Visibility testing
            const b2AABB& aabb = flist->GetAABB(0);
            SDL_Rect aabb1 = {
                (int)((aabb.GetCenter().x - aabb.GetExtents().x)*PPM),
                (int)((aabb.GetCenter().y - aabb.GetExtents().y)*PPM),
                (int)(aabb.GetExtents().x*2.0f*PPM),
                (int)(aabb.GetExtents().y*2.0f*PPM)
            };
            SDL_Rect aabb2 = {cam->pos.x,cam->pos.y,cam->size.x,cam->size.y};
            if(!SDL_HasIntersection(&aabb1,&aabb2)){return;}//Not visible
            //Render current shape at relative position to cam
            b2Vec2 dstPos = {(pos.x*PPM - (float)cam->pos.x)/PPM,(pos.y*PPM - (float)cam->pos.y)/PPM};
            RenderShape(texture,srcRect,flist->GetShape(),dstPos,body->GetAngle(),z);
            flist = flist->GetNext();
        }
    }
    static void RenderColour(uint32_t colour,const SDL_Rect& dst,uint8_t z){
        getRenderQueue()->emplace(nullptr,SDL_Rect({0,0,0,0}),dst,0.0f,SDL_FLIP_NONE,colour,z);
    }
    //Render all items in the queue
    static void Render(){
        SDL_Renderer* renderer = getRenderer();
        priority_queue<RenderQueueItem,vector<RenderQueueItem>,greater<RenderQueueItem>> *queue = getRenderQueue();
        while(!queue->empty()){
            const RenderQueueItem& i = queue->top();
            if(i.texture){
                bool srcNull = i.src.x == 0 && i.src.y == 0 && i.src.w == 0 && i.src.h == 0;
                SDL_RenderCopyEx(renderer,i.texture,srcNull ? nullptr : &i.src,&i.dst,i.angle,nullptr,i.mirror);
            }else{
                uint8_t r = (i.colour & 0xFF000000)>>24;
                uint8_t g = (i.colour & 0x00FF0000)>>16;
                uint8_t b = (i.colour & 0x0000FF00)>>8;
                uint8_t a = (i.colour & 0x000000FF);
                SDL_SetRenderDrawColor(renderer,r,g,b,a);
                SDL_RenderFillRect(renderer,&i.dst);
            }
            queue->pop();
        }
    }

    private:
    struct RenderQueueItem{
        SDL_Texture* texture;
        SDL_Rect src,dst;
        float angle;
        SDL_RendererFlip mirror;//WIP
        uint32_t colour;
        uint8_t z;
        friend auto operator<=>(const RenderQueueItem& l,const RenderQueueItem& r){return l.z<=>r.z;}
    };
    priority_queue<RenderQueueItem,vector<RenderQueueItem>,greater<RenderQueueItem>> renderQueue;
    static priority_queue<RenderQueueItem,vector<RenderQueueItem>,greater<RenderQueueItem>>* getRenderQueue(){return &getInstance<RenderEngine>().renderQueue;}

    vector<Resources::Types> res_enums = {Resources::PNG,Resources::TTF,Resources::FONT,Resources::TEXTURES,Resources::RENDERER,Resources::WINDOW,Resources::SDL};
    
    //Private functions
    //Init functions for resources (deinit/check functions are inlined in RenderEngine::Init function)
    static void* Init_SDL(){
        if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO)<0 || !SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")){
            Logger::logERROR("SDL could not initialize! SDL_Error:  "+string(SDL_GetError()));
            return new bool(false);
        }
        return new bool(true);
    }
    static void* Init_WINDOW(){
        return SDL_CreateWindow("SPACEGAME",SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,
                            Settings::getAs<uint32_t>(Settings::SCREEN_WIDTH),
                            Settings::getAs<uint32_t>(Settings::SCREEN_HEIGHT),
                            SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
            //Logger::logERROR("Window could not be created! SDL_Error: "+string(SDL_GetError()));
    }
    static void* Init_RENDERER(){
        return SDL_CreateRenderer(RenderEngine::getWindow(),-1,SDL_RENDERER_ACCELERATED);
            //Logger::logERROR("Renderer could not be created! SDL_Error: "+string(SDL_GetError()));
    }
    static void* Init_PNG(){
        uint32_t imgFlags = IMG_INIT_PNG;
        return new bool(IMG_Init(imgFlags) & imgFlags);
            //Logger::logERROR("SDL_image could not initialize! SDL_image Error: "+string(IMG_GetError()));
    }
    static void* Init_TTF(){
        return new bool(TTF_Init() != -1);
            //Logger::logERROR("SDL_ttf could not initialize! SDL_ttf Error: "+string(TTF_GetError()));
    }
};