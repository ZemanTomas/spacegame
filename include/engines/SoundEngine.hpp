#pragma once
#include "utils/Engine.hpp"

using namespace std;

struct SEOptions : Options{

};
//Sound engine, provides audio system initialization
class SoundEngine : public Engine{
    public:
    //Virtual engine interface
    void Init(Options& _o){
        [[maybe_unused]] SEOptions& o = static_cast<SEOptions&>(_o);
        //Parse options
        //Init resources
        //Audio initialization, resource is bool
        Resources::AddResource(new Resource(
                &Init_AUDIO,
                [](void* res){  Mix_CloseAudio();
                                Mix_Quit();
                                (*static_cast<bool*>(res)) = false;
                                },
                [](void* res){return res && (*static_cast<bool*>(res));}
                ),
            Resources::AUDIO,true
        );

    }
    void DeInit(Options& _o){
        [[maybe_unused]] SEOptions& o = static_cast<SEOptions&>(_o);
        for(auto& r : res_enums){
            Resources::Dealloc(r);
        }
    }
    void ReInit(Options& _o){//TODO reinit
        [[maybe_unused]] SEOptions& o = static_cast<SEOptions&>(_o);

    }
    bool Inited(){
        for(auto& r : res_enums){
            if(!Resources::Allocated(r)){return false;}
        }
        return true;
    }
    //User interface
    private:
    vector<Resources::Types> res_enums = {Resources::AUDIO};
    //Private functions
    //Initialize audio
    static void* Init_AUDIO(){
        try{
            int32_t mixFlags = MIX_INIT_OGG|MIX_INIT_MP3;
            if(Mix_Init(mixFlags) != mixFlags){
                Logger::logERROR("Audio Mixer could not initialize! Mix_Error:  "+string(Mix_GetError()));
                return new bool(false);
            }
            if(Mix_OpenAudio(44100,MIX_DEFAULT_FORMAT,2,2048) < 0){
                Logger::logERROR("Mixer could not open! Mix_Error:  "+string(Mix_GetError()));
                return new bool(false);
            }
        }catch(const exception &e){
            return new bool(false);
        }
        return new bool(true);
    }
};