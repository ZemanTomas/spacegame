#pragma once
#include "utils/Engine.hpp"

using namespace std;

struct IEOptions : Options{

};
//InputEngine, providing input handling functions, currently none
class InputEngine : public Engine{
    public:
    //Virtual engine interface
    void Init(Options& _o){
        [[maybe_unused]] IEOptions& o = static_cast<IEOptions&>(_o);
        //Parse options
        //Init resources

    }
    void DeInit(Options& _o){
        [[maybe_unused]] IEOptions& o = static_cast<IEOptions&>(_o);

    }
    void ReInit(Options& _o){
        [[maybe_unused]] IEOptions& o = static_cast<IEOptions&>(_o);

    }
    bool Inited(){
        return true;
    }
    //User interface
    private:
    //Private functions
};