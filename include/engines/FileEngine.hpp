#pragma once
#include <string>
#include <SDL.h>
#include "utils/Engine.hpp"
#include "utils/Logger.hpp"

using namespace std;

struct FEOptions : Options{

};

//TODO seriazable interface
//FileEngine, providing additional file abstraction functions
class FileEngine : public Engine{
    public:
    //Virtual engine interface
    void Init(Options& _o){
        [[maybe_unused]] FEOptions& o = static_cast<FEOptions&>(_o);
        //Parse options
        //Init resources

    }
    void DeInit(Options& _o){
        [[maybe_unused]] FEOptions& o = static_cast<FEOptions&>(_o);

    }
    void ReInit(Options& _o){
        [[maybe_unused]] FEOptions& o = static_cast<FEOptions&>(_o);

    }
    bool Inited(){
        return true;
    }
    //User interface
    //Low level byte saving functions saves n bytes to file path "name"
    static bool saveBytes(const char* bytes, size_t n, string name, bool append = false){
        Logger::logINFO("Saving "+to_string(n)+" bytes to "+name);
        SDL_RWops *rw = SDL_RWFromFile(name.c_str(), append ? "ab" : "wb");
        if(rw == nullptr){return false;}
        if(SDL_RWwrite(rw,bytes,1,n) != n){//Did not write n bytes
            SDL_RWclose(rw);
            Logger::logINFO("Byte write failure!");
            return false;
        }
        SDL_RWclose(rw);
        return true;
        
    }
    //Load up to m bytes from file path "name" to byte array res
    static size_t loadBytes(char* res, string name, size_t m = SIZE_MAX){
        SDL_RWops *rw = SDL_RWFromFile(name.c_str(), "rb");
        if(rw == nullptr){return 0;}
        int64_t size = SDL_RWsize(rw);
        Logger::logINFO("Loading "+to_string(size)+" bytes from "+name);
        if(size < 0){return 0;}
        
        //Don't load more than filesize
        if(m < (uint64_t)size){size = m;}
        
        char* buf = res;//For easier writing of \0 at end
        int64_t read_total = 0,read = 1;

        while(read_total < size && read != 0){//Read file in loop
            read = SDL_RWread(rw, buf, 1,size - read_total);
            read_total += read;
            buf += read;
        }
        SDL_RWclose(rw);
        res[read_total] = '\0'; 
        return read_total;
    }
    //Returns true if file exists
    static bool fileExists(string name){
        SDL_RWops *rw = SDL_RWFromFile(name.c_str(),"r");
        SDL_RWclose(rw);
        return rw != nullptr;
    }
    //Returns size of file in bytes (-1 on error)
    static int64_t fileSize(string name){
        SDL_RWops *rw = SDL_RWFromFile(name.c_str(),"r");
        if(rw == nullptr){return -1;}
        int64_t res = SDL_RWsize(rw);
        SDL_RWclose(rw);
        return res;
    }
    private:
    //Private functions
};