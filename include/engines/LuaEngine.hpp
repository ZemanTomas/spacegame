#pragma once
#include "utils/Engine.hpp"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <LuaState.h>
#pragma GCC diagnostic pop

using namespace std;

struct LEOptions : Options{

};
//LuaEngine, provides simple interface for scripting in Lua
class LuaEngine : public Engine{
    public:
    //Virtual engine interface
    void Init(Options& _o){
        [[maybe_unused]] LEOptions& o = static_cast<LEOptions&>(_o);
        //Parse options
        //Init


    }
    void DeInit(Options& _o){
        [[maybe_unused]] LEOptions& o = static_cast<LEOptions&>(_o);

    }
    void ReInit(Options& _o){
        [[maybe_unused]] LEOptions& o = static_cast<LEOptions&>(_o);

    }
    bool Inited(){
        return true;
    }
    //User interface
    lua::State state;
    lua::State& getState(){return state;}
    //Execute a string
    void execute(const string& cmd){state.doString(cmd);}
    //Execute a script from file
    void executeFile(const string& path){state.doFile(path);}
    private:
    //Private functions
};