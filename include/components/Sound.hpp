#pragma once
#include "ecs/Manager.hpp"
#include "utils/Utils.hpp"
#include <SDL_mixer.h>
using namespace std;
//Possible actions, chunk play once, chunk play x times, chunk play repeatedly, music play, music pause, music fade out
enum SoundAction : uint8_t{
    CHUNK_PLAY = 0x1,
    CHUNK_PLAY_TIMES = 0x2,
    CHUNK_PLAY_INFINITE = 0x4,

    MUSIC_PLAY = 0x8,
    MUSIC_PAUSE = 0x10,
    MUSIC_FADE_MS = 0x20
};
//Sound Component, contains Chunk and Music, action is bit array, affecting chunk and music independently
struct SoundComponent : public Component{
    static const ComponentType ctype;
    Mix_Chunk* chunk;
    Mix_Music* music;
    uint8_t action;
    int param;
    SoundComponent(Mix_Chunk* _chunk = NULL, SoundAction _action = CHUNK_PLAY, int _param = 0):chunk(_chunk),action(_action),param(_param){}
    SoundComponent(Mix_Music* _music = NULL, SoundAction _action = MUSIC_PLAY, int _param = 0):music(_music),action(_action),param(_param){}
};