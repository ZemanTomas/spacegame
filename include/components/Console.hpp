#pragma once
#include "ecs/Manager.hpp"
#include "utils/Utils.hpp"
#include <string>
using namespace std;
//Console Component - stores default prompt and current console commandline
struct ConsoleComponent : public Component{
    static const ComponentType ctype;
    string prompt,text;
    //Constructor, default prompt
    ConsoleComponent(string _prompt):prompt(_prompt){}
};