#pragma once
#include "ecs/Manager.hpp"
#include "utils/Utils.hpp"
using namespace std;
//Type Component - arbitrary byte for general purposes
struct TypeComponent : public Component{
    static const ComponentType ctype;
    uint8_t t;
    TypeComponent(uint8_t _t):t(_t){}
};