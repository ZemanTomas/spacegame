#pragma once
#include "ecs/Manager.hpp"
#include "utils/Utils.hpp"
using namespace std;
//Sprite Component, either texture or colour
struct SpriteComponent : public Component{//TODO check ordering
    static const ComponentType ctype;
    //source rect (in texture space)
    SDL_Rect src;
    //dest rect (relative or global depending on other components)
    SDL_Rect dst;
    //Texture
    Texture *texture;
    //Colour background/if texture missing
    uint32_t colour;
    //Angle
    float angle;
    //Flip(mirror)
    bool mirror;
    //Bound to physics body
    bool body;
    //Z order
    uint8_t z;
    //Constructor, texture, source rect, bound to body, z order, mirror texture, destination(x,y,w,h) (relative to body or global), angle
    SpriteComponent(Texture *_t,SDL_Rect _s, bool _b = true, uint8_t _z = 0, bool _m = false, SDL_Rect _d = {0,0,0,0}, float _a = 0):
        src(_s),
        dst(_d),
        texture(_t),
        colour(0x000000FF),
        angle(_a),
        mirror(_m),
        body(_b),//TODO: set bound to body default to false or smarter
        z(_z)
    {
        SetAnimation({texture->getWidth(),texture->getHeight()},{0,0},0);
    }
    //Constructor, colour, bound to body, z order, mirror, destination (relative to body or global), angle
    SpriteComponent(uint32_t _c, bool _b = true, uint8_t _z = 0, bool _m = false, SDL_Rect _d = {0,0,0,0}, float _a = 0):
        src({0,0,0,0}),
        dst(_d),
        texture(nullptr),
        colour(_c),
        angle(_a),
        mirror(_m),
        body(_b),
        z(_z)
    {
        SetAnimation({0,0},{0,0},0);
    }
    //Sets animation, texture is considered as a tilemap, tilesize is a size of each tile, start is first tile coordinates, frames is number of animation frames (must be >0), frameticks is duration of each frame, offsets for animation timing
    void SetAnimation(int_2D _tilesize, int_2D _start, uint32_t _frames, uint32_t _frameticks = 60, uint32_t _frameoffset = 0, uint32_t _tickoffset = 0){
        tilesize = _tilesize;
        start = _start;
        frames = _frames;
        frameticks = _frameticks;
        frameoffset = _frameoffset;
        tickoffset = _tickoffset;
        currenttick = 0;
    }
    //Unsets animation
    void UnsetAnimation(){
        frames = 0;
    }
    //Returns current source rectangle in texture space (tick important for animations)
    SDL_Rect getCurrentSrc(const uint64_t tick = 0) const {
        if(frames == 0){return src;}
        int x = start.x*tilesize.x;
        int y = start.y*tilesize.y;
        int currentframe = (frameoffset + ((tickoffset + tick) / frameticks)) % frames;
        int actualx = x + currentframe * tilesize.x;
        int newy = y + (actualx / texture->getWidth())*tilesize.y;
        int newx = actualx % texture->getWidth();


        return {newx,newy,tilesize.x,tilesize.y};
    }
    int_2D tilesize;
    int_2D start;
    uint32_t frames;
    uint32_t frameticks;//How many ticks frame stays on
    uint32_t frameoffset;
    uint32_t tickoffset;
    uint64_t currenttick;//TODO save space
    //TODO tick-independent
};