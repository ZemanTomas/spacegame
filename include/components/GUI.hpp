#pragma once
#include <string>
#include <functional>
#include <vector>

#include "imgui/imgui.h"

#include "ecs/Manager.hpp"
#include "utils/Utils.hpp"
using namespace std;
//GUI Component - window name imGui function, vector of pointers to variables and arbitrary pointer for parameter passing
struct GUIComponent : public Component{
    static const ComponentType ctype;
    public:
    //Constructor, window name, function to call every loop (containing imgui call), pointer for passing parameters
    GUIComponent(string _name, function<vector<void*>(string, void*)> _f, void* _p = nullptr):name(_name),doGui(_f),params(_p){}
    string name;
    function<vector<void*>(string,void*)> doGui;
    vector<void*> values;
    void* params;
};