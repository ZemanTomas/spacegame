#pragma once
#include "ecs/Manager.hpp"
#include "utils/Utils.hpp"
#include <box2d/box2d.h>
using namespace std;
//Physics Component
struct PhysicsComponent : public Component{//TODO: Separate B2D and other physics
    static const ComponentType ctype;
    public:
    //One of 4 types, solid -> not moving, dynamic -> moving, B2D -> has box2D body and uses it's physics, not B2D -> uses simple physics
    enum Type : uint8_t {
        Solid,
        Dynamic,
        Solid_B2D,
        Dynamic_B2D
    } ptype;//Solid/Solid_B2D/Dynamic_B2D
    //Structured options, angle, density, friction, bounciness, max speed
    struct PhysicsOpts{float angle,density,friction,bounciness,maxSpeed;};
    //Solid object
    PhysicsComponent(float_2D _pos, float_2D _size):ptype(Solid),pos(_pos),collider({_size,TL}){}
    //Dynamic object with velocity _vel
    PhysicsComponent(float_2D _pos, float_2D _size, float_2D _vel):ptype(Dynamic),pos(_pos),vel(_vel),collider({_size,TL}){}
    //B2D object, can be circle or rectangle
    PhysicsComponent(b2World* world,float_2D _pos,float_2D _size,bool dynamic = true, bool circle = false, PhysicsOpts opts = {0.0f,0.0f,0.0f,0.0f,15.0f}){
        pos = _pos;_pos/=PPM;
        collider.size = _size;_size/=PPM;

        maxSpeed = opts.maxSpeed;


        //Box2D body definition
        b2BodyDef bdef;
        b2FixtureDef fixture;
        bdef.angle = opts.angle;
        bdef.position.Set(_pos.x,_pos.y);

        b2PolygonShape shapep;
        b2CircleShape shapec;
        if(circle){
            shapec.m_radius = _size.x/2.0f;
            fixture.shape = &shapec;
        }else{
            shapep.SetAsBox(_size.x/2.0f,_size.y/2.0f);
            fixture.shape = &shapep;
        }
        if(dynamic){//Dynamic body
            ptype = Dynamic_B2D;

            bdef.allowSleep = true;
            //bdef.bullet = true;
            bdef.type = b2_dynamicBody;
            body = world->CreateBody(&bdef);
            fixture.density = opts.density;
            fixture.friction = opts.friction;
            fixture.restitution = opts.bounciness;
            body->CreateFixture(&fixture);
            body->SetFixedRotation(true);
        }else{//Solid body
            ptype = Solid_B2D;

            bdef.type = b2_staticBody;
            bdef.allowSleep = true;
            body = world->CreateBody(&bdef);
            if(circle){
                body->CreateFixture(&shapec,opts.density);
            }else{
                body->CreateFixture(&shapep,opts.density);
            }
        }
        //Set user data to 0
        body->GetUserData().pointer = (uintptr_t)0;
    }
    //Getters/Setters, depending on type, use values in this class directly or in Box2D body
    float_2D getSize(){return collider.size;}
    float_2D getPos()const{if(ptype==Solid||ptype==Dynamic){return pos;}else{return float_2D::toFloat_2D(body->GetPosition());}}
    float_2D getVel()const{if(ptype==Dynamic_B2D||ptype==Solid_B2D){return float_2D::toFloat_2D(body->GetLinearVelocity());}return vel;}
    b2Body* getBody(){return body;}

    void setPos(float_2D _pos){pos = _pos;if(ptype==Dynamic_B2D||ptype==Solid_B2D){body->SetTransform({_pos.x,_pos.y},body->GetAngle());}}
    void setVel(float_2D _vel){vel = _vel;if(ptype==Dynamic_B2D||ptype==Solid_B2D){body->SetLinearVelocity({_vel.x,_vel.y});}}

    float_2D pos,vel;
    //Alignment of object : Center/TopLeft, currently unused
    enum Alignment{C,TL};
    //Collider for simple physics
    struct Collider{
        float_2D size;
        Alignment alignment;
    } collider;
    b2Body* body;
    float maxSpeed;
};