#pragma once
#include "ecs/Manager.hpp"
#include "utils/Utils.hpp"
using namespace std;
//Input Component, marking entity as an input entity
struct InputComponent : public Component{
    static const ComponentType ctype;
    bool focus;
    InputComponent(bool _focus = false):focus(_focus){}
};