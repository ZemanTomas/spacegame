#pragma once
#include <unordered_map>
#include <vector>
#include <string>
#include <getopt.h>

class argParser{
    public:
    argParser(int _argc, char** _argv, bool _evalAlways = false):
        argc(_argc),
        argv(_argv),
        evalAlways(_evalAlways)
    {}
    void addOptionLong(const std::string _name,bool arg = false,char s = 0, const std::string help = ""){
        std::string name(_name);
        char *namec = (char*)malloc((name.size()+1)*sizeof(char));
        strcpy(namec,name.c_str());
        optionW tmp = {{namec,arg,0,s},arg,move(name),move(help)};
        keys.push_back(tmp);
        std::string str_arg = "",str_opt = "";
        if(s!=0){str_opt+=s;if(arg!=0){str_arg=":";}shortopt+=str_opt+str_arg;}
        values[_name]="";
        if(evalAlways){eval();}
    }
    bool isSet(std::string name){
        return values.find(name) != values.end();
    }
    std::string get(std::string name){
        return isSet(name)?values[name]:"";
    }
    void set(std::string name,std::string val = ""){
        if(isSet(name)){values[name]=val;}
    }
    void eval(){
        values.clear();
        int c;
        int digit_optind = 0;
        option *long_options = new option[keys.size()+1];
        for(optionW op : keys){
            long_options[digit_optind++] = op.o;
        }
        long_options[digit_optind] = {0,0,0,0};
        digit_optind = 0;

        while (1) {
            int option_index = 0;

            c = getopt_long(argc, argv, shortopt.c_str(),
                    long_options, &option_index);
            if (c == -1){break;}
            for(optionW op : keys){
                if(op.o.val == c){
                    if(op.arg){//TODO Ternary Operator
                        values[op.s]=optarg;
                    }else{
                        values[op.s]="";
                    }
                    break;
                }
            }
        }
        delete[] long_options;
    }
    std::string getHelp(){
        std::string ret = "Usage:";
        for(optionW x : keys){if(x.help.size() > 0){ret += "\n"+x.help;}}
        return ret;
    }
    //TODO: deallocate
    private:
    struct optionW{
        option o;
        bool arg;
        std::string s = "";
        std::string help ="";
    };
    std::unordered_map<std::string,std::string> values; //Only longest name
    std::vector<optionW> keys;
    int argc;
    char **argv;
    bool evalAlways;
    std::string shortopt = "";
};
