#pragma once
#include <chrono>
#include <array>
#include <string>
#include <utility>
#include <SDL_timer.h>

using namespace std::chrono;

//TODO: create inheritable timer class

const std::uint64_t PERFORMANCE_FREQUENCY = SDL_GetPerformanceFrequency();
const double PERFORMANCE_FREQUENCY_D = (double)PERFORMANCE_FREQUENCY;

class SimpleTimer{
    std::uint64_t t;
    public:
    SimpleTimer():t(){measure();}
    //Set t to current time
    void measure(){t = SDL_GetPerformanceCounter();}
    //Get time since last measure //TODO: constexpr
    template<class T> std::uint64_t total(){return (std::uint64_t)(((double)T::den/PERFORMANCE_FREQUENCY_D)*(SDL_GetPerformanceCounter() - total_raw()));}
    template<class T> double totalD(){return ((double)T::den/PERFORMANCE_FREQUENCY_D)*(SDL_GetPerformanceCounter() - total_raw());}
    std::uint64_t total_raw(){return t;}
};

class StoppableTimer : public SimpleTimer{
    //Saved time
    std::uint64_t sum;
    public:
    StoppableTimer():SimpleTimer(),sum(){measure();}
    //Starts measuring time
    void Start(){measure();}
    //Increments saved time by time since last Start() call
    void Stop(){sum += total<std::nano>();}
    //Erases saved time. Does not affect running timer from Start() call
    void Erase(){sum = 0;}
    //Resets saved time and starts measuring time
    void Restart(){Erase();Start();}

    //Get saved time
    template<class T> std::uint64_t get()const{return (std::uint64_t)(((double)T::den/PERFORMANCE_FREQUENCY_D)*get_raw());}
    template<class T> double getD()const{return ((double)T::den/PERFORMANCE_FREQUENCY_D)*get_raw();}
    std::uint64_t get_raw()const{return sum;}
};

class DelayTimer{
    SimpleTimer delay_timer;
    public:
    DelayTimer():delay_timer(){}
    //delay functions
    void delay(std::uint64_t _ns){
        if(_ns <= 1){return;}
        delay_timer.measure();
        while(delay_timer.total<nano>() < _ns){}
    }
};

//Class managing multiple timers
template<std::size_t N>
class Timers : public DelayTimer{
    std::array<SimpleTimer,N> t;
    public:
    Timers():DelayTimer(),t(){}
    void measure(std::size_t timer){t[timer].measure();}
    template<class T> std::uint64_t total(std::size_t timer){return t[timer].template total<T>();}
    template<class T> double totalD(std::size_t timer){return t[timer].template totalD<T>();}
    std::uint64_t total_raw(std::size_t n){return t[n].total_raw();}
};

//Interface:
/*{Reset,Start,Get,Stop}<N*>
Report()
SetFormat()
Name<N>()
Desc<N>()
StopAndStart<N,N>
Delay()
*/

//Class managing multiple stoppable timers
template<std::size_t N>
class PerfTimer : public DelayTimer{
    std::array<StoppableTimer,N> t;
    public:
    PerfTimer():DelayTimer(),t(){}
    
    //TODO: synchronize all measurements in var. template functions to use one time
    //TODO: check if id < N
    //TODO: report generation

    template <std::size_t id> void Start(){t[id].Start();}
    template <std::size_t id, std::size_t id2, std::size_t... ids> void Start(){Start<id>();Start<id2,ids...>();}
    template <std::size_t... ids> void Start(std::index_sequence<ids...> seq){Start<ids...>();}
    void StartAll(){Start(std::make_index_sequence<N>());}

    template <std::size_t id> void Stop(){t[id].Stop();}
    template <std::size_t id, std::size_t id2, std::size_t... ids> void Stop(){Stop<id>();Stop<id2,ids...>();}
    template <std::size_t... ids> void Stop(std::index_sequence<ids...> seq){Stop<ids...>();}
    void StopAll(){Stop(std::make_index_sequence<N>());}

    template <std::size_t id> void Erase(){t[id].Erase();}
    template <std::size_t id, std::size_t id2, std::size_t... ids> void Erase(){Erase<id>();Erase<id2,ids...>();}
    template <std::size_t... ids> void Erase(std::index_sequence<ids...>){Erase<ids...>();}
    void EraseAll(){Erase(std::make_index_sequence<N>());}

    template <std::size_t id> void Restart(){t[id].Restart();}
    template <std::size_t id, std::size_t id2, std::size_t... ids> void Restart(){Restart<id>();Restart<id2,ids...>();}
    template <std::size_t... ids> void Restart(std::index_sequence<ids...> seq){Restart<ids...>();}
    void RestartAll(){Restart(std::make_index_sequence<N>());}

    //Total since last start
    template<class T> std::uint64_t total(std::size_t timer){return t[timer].template total<T>();}
    template<class T> double totalD(std::size_t timer){return t[timer].template totalD<T>();}
    std::uint64_t total_raw(std::size_t n){return t[n].total_raw();}

    //Get saved time
    template<class T> std::uint64_t get(std::size_t timer){return t[timer].template get<T>();}
    template<class T> double getD(std::size_t timer){return t[timer].template getD<T>();}
    std::uint64_t get_raw(std::size_t n){return t[n].get_raw();}


    template <std::size_t stopId, std::size_t startId> void StopAndStart(){Stop<stopId>();Start<startId>();}
};