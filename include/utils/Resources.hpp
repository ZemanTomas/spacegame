#pragma once
#include <vector>
#include <map>
#include <stdint.h>
#include "platform/Filesystem.hpp"

#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <SDL_mixer.h>

#include "utils/Settings.hpp"
#include "utils/Logger.hpp"

using namespace std;
//A pointer to resource and function pointers for: allocating resource, deallocating and function that checks if resource is allocated
struct Resource{//TODO Error
    //TODO use smart pointers
    //Default constructor, empty resource
    Resource():alloc(NULL),res(NULL),dealloc(NULL),is([]([[maybe_unused]] void* res){return false;}){}
    Resource(void*(*_a)(void),void (*_d)(void*),bool (*_is)(void*)):alloc(_a),res(NULL),dealloc(_d),is(_is){}
    void* (*alloc)(void);
    void* res;
    void (*dealloc)(void*);
    bool (*is)(void*);
    void allocate(){res = alloc();}
    bool isAllocated(){return is(res);}
    ~Resource(){if(isAllocated()){dealloc(res);}}
    Resource(Resource&) = delete;
};

//TODO common naming with engine

//Global singleton class, resource storage, enumeration, initialization
class Resources{
    public:
    //Enum of predefined resources
    enum Types : uint8_t{//TODO get rid of enum
        SDL,
        WINDOW,
        RENDERER,
        PNG,
        AUDIO,
        TTF,
        FONT,
        TEXTURES,
        TYPES_TOTAL
    };
    //Enum of predefined textures
    enum Textures : uint8_t {
        PLAYER_TEXTURE,
        MAP_TEXTURE,
        TEXTURES_TOTAL
    };
    
    static Resources& getInstance(){
        static Resources instance;
        return instance;
    }
    const void*  operator[](size_t i) const {return db.at(i)->res;}
          void*& operator[](size_t i)       {return db[i]->res;}
    //
    //Does default resource exist
    static bool isSet(Types i){return getInstance()[i] == NULL;}
    //Adds resource with custom id (must be higher than TYPES_TOTAL, else it overwrites a default resource)
    static bool AddResource(Resource* res, size_t id, bool allocate = false){return getInstance().Add(res,id,allocate);}
    //Initialize (allocate) all resources
    static bool Initialize(){return getInstance().Init();}
    //Allocate resource
    static void Allocate(size_t id){getInstance().Alloc(id);}
    //Is resource allocated
    static bool Allocated(size_t id){return getInstance().isAllocated(id);}
    //Does resource exist
    static bool Contains(size_t id){return getInstance().isSet(id);}
    //Deallocate resource
    static void Dealloc(size_t id){getInstance().Free(id);}
    //Deallocate all rsources
    static void Dealloc(){getInstance().Free();}
    //TODO get free id
    //Allocate all resources
    bool Init(){
        //Allocate all
        for(pair<size_t,Resource*> r : db){
            if(!r.second->isAllocated()){r.second->allocate();}
            if(!r.second->isAllocated()){return false;}
        }
        return true;
    }
    //Add resource
    bool Add(Resource* res,size_t id, bool allocate = false){
        if(isSet(id)){;Free(id);}
        //db[id] = res; //For some reason this caused a lot of errors upon game exit
        db.insert(make_pair(id,res));
        if(allocate){Alloc(id);}
        return db[id]->isAllocated();
    }
    //Allocate resource
    void Alloc(size_t id){
        if(isSet(id)){db[id]->allocate();}
    }
    //Free all resources
    void Free(){
        for(auto it = db.rbegin(); it!= db.rend(); ++it){
            Free(it->first);
        }
    }
    //Free resource
    void Free(size_t id){
        if(isSet(id)){delete db[id];db.erase(id);}
    }
    //Does resource exist
    bool isSet(size_t id){
        return db.contains(id);
    }
    //Is resource allocated
    bool isAllocated(size_t id){
        return isSet(id) && db[id]->isAllocated();
    }
    private:
    map<size_t,Resource*> db = map<size_t,Resource*>();
    Resources() = default;
    ~Resources() = default;
    Resources(const Resources&) = delete;
    Resources operator&(const Resources&) = delete;
    //
    
    

};