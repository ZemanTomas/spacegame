#pragma once
//TODO deprecate
//Useful types

struct float_2D{
    float x,y;
    float_2D& operator+=(const float_2D& f){x+=f.x;y+=f.y; return *this;}
    friend float_2D operator+(float_2D l, const float_2D& r){l+=r;return l;}
    float_2D& operator-=(const float_2D& f){x-=f.x;y-=f.y; return *this;}
    friend float_2D operator-(float_2D l, const float_2D& r){l-=r;return l;}
    float_2D& operator*=(const float_2D& f){x*=f.x;y*=f.y; return *this;}
    friend float_2D operator*(float_2D l, const float_2D& r){l*=r;return l;}
    float_2D& operator/=(const float_2D& f){x/=f.x;y/=f.y; return *this;}
    friend float_2D operator/(float_2D l, const float_2D& r){l/=r;return l;}

    float_2D& operator+=(const float& f){x+=f;y+=f; return *this;}
    friend float_2D operator+(float_2D l, const float& r){l+=r;return l;}
    float_2D& operator-=(const float& f){x-=f;y-=f; return *this;}
    friend float_2D operator-(float_2D l, const float& r){l-=r;return l;}
    float_2D& operator*=(const float& f){x*=f;y*=f; return *this;}
    friend float_2D operator*(float_2D l, const float& r){l*=r;return l;}
    float_2D& operator/=(const float& f){x/=f;y/=f; return *this;}
    friend float_2D operator/(float_2D l, const float& r){l/=r;return l;}

    static float_2D toFloat_2D(const b2Vec2 x){return {x.x,x.y};}//TODO FIX
    static b2Vec2 tob2Vec2(const float_2D x){return b2Vec2(x.x,x.y);}//TODO FIX
};
struct int_2D{
    int x,y;
    int_2D& operator+=(const int_2D& f){x+=f.x;y+=f.y; return *this;}
    friend int_2D operator+(int_2D l, const int_2D& r){l+=r;return l;}
    int_2D& operator-=(const int_2D& f){x-=f.x;y-=f.y; return *this;}
    friend int_2D operator-(int_2D l, const int_2D& r){l-=r;return l;}
    int_2D& operator*=(const int_2D& f){x*=f.x;y*=f.y; return *this;}
    friend int_2D operator*(int_2D l, const int_2D& r){l*=r;return l;}
    int_2D& operator/=(const int_2D& f){x/=f.x;y/=f.y; return *this;}
    friend int_2D operator/(int_2D l, const int_2D& r){l/=r;return l;}
    
    int_2D& operator+=(const int& f){x+=f;y+=f; return *this;}
    friend int_2D operator+(int_2D l, const int& r){l+=r;return l;}
    int_2D& operator-=(const int& f){x-=f;y-=f; return *this;}
    friend int_2D operator-(int_2D l, const int& r){l-=r;return l;}
    int_2D& operator*=(const int& f){x*=f;y*=f; return *this;}
    friend int_2D operator*(int_2D l, const int& r){l*=r;return l;}
    int_2D& operator/=(const int& f){x/=f;y/=f; return *this;}
    friend int_2D operator/(int_2D l, const int& r){l/=r;return l;}
    
    static int_2D toInt_2D(const float_2D x){return {(int)x.x,(int)x.y};}
    static float_2D toFloat_2D(const int_2D x){return {(float)x.x,(float)x.y};}
};