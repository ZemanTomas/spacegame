#pragma once
#include "utils/Types.hpp"

//Helping class for calculating coords relative to camera
struct Camera{
    //Constructor(global position, size of camera viewport)
    Camera(int_2D _p = {0,0}, int_2D _s = {0,0}):pos(_p),size(_s){}
    int_2D pos; //Global,TL
    int_2D size;
    float ZOOM_LEVEL = 1.0f;
    //Gets relative position to current camera location
    int_2D getRelativePos(const int_2D& _p) {return {_p.x-pos.x,_p.y-pos.y};}//TODO toWorld, toLocal
    //Returns true if rect (at _p, of size _s) is visible with camera
    bool visible(const int_2D& _p, const int_2D& _s)  {
        const int_2D rel = getRelativePos(_p);
        const bool visX = rel.x+_s.x >= 0 && rel.x <= size.x;
        const bool visY = rel.y+_s.y >= 0 && rel.y <= size.y;
        return visX && visY;
    }
    void centerAround(const int_2D& _p){
        pos = {_p.x - size.x/2,_p.y - size.y/2};
    }
};
