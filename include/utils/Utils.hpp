#pragma once
#include <string>
#include <iostream>
#include <sstream>
#include <cmath>
#include <SDL.h>

#include "utils/Texture.hpp"
#include <box2d/box2d.h>

const float PPM = 1.2f;//PixelPerMeter, for Box2D physics
//const uint32_t PPM_UINT = 1U;
//const int32_t PPM_INT = 1;

#include "utils/Types.hpp"
#include "utils/Camera.hpp"
#include "utils/Logger.hpp"

using namespace std;

//Simple collider physics
inline void CollideFloat(float_2D& pos, const float_2D& col, float_2D& vel, const float_2D& tl, const float_2D& br){//TODO deprecate
    //X
    if(pos.x < tl.x){
        pos.x = tl.x;
        vel.x *= -1.0f;
    }else if(pos.x+col.x > br.x){
        pos.x = br.x-col.x;
        vel.x *= -1.0f;
    }
    //Y
    if(pos.y < tl.y){
        pos.y = tl.y;
        vel.y *= -1.0f;
    }else if(pos.y+col.y > br.y){
        pos.y = br.y-col.y;
        vel.y *= -1.0f;
    }
}

//Split string by whitespace
inline vector<string> splitString(string str){
    vector<string> res;
    istringstream ss(str);
    string tok;
    while(ss >> tok){res.push_back(tok);}
    return res;
}

inline bool isAround(int32_t x1,int32_t y1,int32_t x2,int32_t y2,uint32_t range = 1){
    return (abs(x2 - x1) <= range)&&(abs(y2 - y1) <= range);
}

inline string getVersion(){
    return SPACEGAME_VERSION;
}


/*
class b2DrawSDL2 : public b2Draw{
public:
    b2DrawSDL2(Camera* _cam,SDL_Renderer* _renderer):b2Draw(),cam(_cam),renderer(_renderer){SetFlags(0xff);};
    Camera* cam;
    SDL_Renderer* renderer;


    void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)override{
        if(vertexCount < 2 || !cam || !renderer){return;}
        SDL_FPoint* linePoints = new SDL_FPoint[vertexCount];
        for(int i = 0; i < vertexCount; i++){
            b2Vec2 p = {vertices[i].x*PPM, vertices[i].y*PPM};//to world
            b2Vec2 camPos = {cam->pos.x,cam->pos.y};
            linePoints[i] = {p.x - camPos.x,p.y - camPos.y};
        }
        SDL_SetRenderDrawColor(renderer,color.r*255.0f,color.g*255.0f,color.b*255.0f,color.a*255.0f);
        SDL_RenderDrawLinesF(renderer,linePoints,vertexCount);
        SDL_RenderDrawLineF(renderer,linePoints[0].x,linePoints[0].y,linePoints[vertexCount-1].x,linePoints[vertexCount-1].y);
    }
    void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)override{

    }
    void DrawCircle(const b2Vec2& center, float radius, const b2Color& color)override{

    }
    void DrawSolidCircle(const b2Vec2& center, float radius, const b2Vec2& axis, const b2Color& color)override{

    }
    void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color)override{

    }
    void DrawTransform(const b2Transform& xf)override{

    }
    void DrawPoint(const b2Vec2& p, float size, const b2Color& color)override{

    }
};*/