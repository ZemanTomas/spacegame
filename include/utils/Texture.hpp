#pragma once
#include <string>

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

using namespace std;

//Stores SDL_Texture* and provides functions for loading textures/creating texture from text/deallocating
class Texture{
	public:
	Texture():texture(NULL),w(0),h(0){}
	~Texture(){clear();}
	//Load texture from disk, returns true if success
	bool load(string path,SDL_Renderer* renderer){
		clear();
		SDL_Texture* newT = NULL;
		SDL_Surface* surface = IMG_Load(path.c_str());
		if(surface == NULL){/*Could not load*/}else{
			SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 0, 0xFF, 0xFF));
			newT = SDL_CreateTextureFromSurface(renderer, surface);
			if(newT == NULL){//Unable to create texture
			}else{
				w = surface->w;
				h = surface->h;
			}
			SDL_FreeSurface(surface);
		}
		texture = newT;
		return texture != NULL;
	}
	//Create texture from text, returns true on success
	bool loadText(string* text, SDL_Color col, TTF_Font* font,SDL_Renderer* renderer){
		clear();
		SDL_Surface* textSurface = TTF_RenderText_Solid(font, text->c_str(), col);
		if(textSurface != NULL){
			texture = SDL_CreateTextureFromSurface(renderer, textSurface);
			if(texture == NULL){//ERROR
			}else{
				w = textSurface->w;
				h = textSurface->h;
			}
			SDL_FreeSurface(textSurface);
		}else{//Error rendering TTF
		}
		return texture != NULL;
	}
	//Deallocate texture
	void clear(){
		if(texture != NULL){
			SDL_DestroyTexture(texture); //Textures get destroyed by SDL_DestroyRenderer
			texture = NULL;
			w = 0;
			h = 0;
		}
	}
	void setColor(uint8_t red, uint8_t green, uint8_t blue){
		SDL_SetTextureColorMod(texture, red, green, blue);
	}
	void setBlendMode(SDL_BlendMode blending){
		SDL_SetTextureBlendMode(texture, blending);
	}
	void setAlpha(uint8_t alpha){
		SDL_SetTextureAlphaMod(texture, alpha);
	}
	int getWidth(){return w;}
	int getHeight(){return h;}
    SDL_Texture* getTexture(){return texture;}
	private:
	SDL_Texture* texture;
	int w;
	int h;
};