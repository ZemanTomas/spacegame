#pragma once

#include <vector>
//SDL
#include <SDL.h>
#include <SDL_ttf.h>
//Box2D
#include <box2d/box2d.h>

#include "utils/Settings.hpp"
#include "utils/Resources.hpp"
#include "utils/Utils.hpp"
#include "utils/Texture.hpp"
#include "utils/Camera.hpp"
//ECS
#include "ecs/Manager.hpp"

//Engines
#include "engines/InputEngine.hpp"
#include "engines/RenderEngine.hpp"
#include "engines/SoundEngine.hpp"
#include "engines/LuaEngine.hpp"
#include "engines/FileEngine.hpp"

//SceneGraph forward declaration
using SceneID = size_t;

class SceneGraph;

//Forward declaration trick function
//Change scene in scenegraph to sceneID
void ChangeScene(SceneGraph*,SceneID);

using namespace std;

//Scene interface, postinit, deinit, reloadSettings and update functions should be implemented by child classes
class Scene{
    public:
    //Pointer to scenegraph, id of this scene, vector of pairs resourceid <-> resource pointer (resources required by the scene, will get allocated automatically)
    Scene(SceneGraph* _graph, SceneID _id, vector<pair<size_t,Resource*>>* _res = NULL):id(_id),graph(_graph),res(_res){}
    const SceneID id;
    SceneGraph* graph;
    Manager manager;
    vector<SDL_Event> events; //vector of events for each cycle
    vector<pair<size_t,Resource*>>* res;
    //Change scenes
    void Change(SceneID to){ChangeScene(graph,to);}
    //Initialize this scene
    void Init(){PreInit();PostInit();}
    //Allocate needed resources
    void PreInit(){
        if(res == NULL){return;}
        for(pair<size_t,Resource*> r : *res){
            if(!Resources::Contains(r.first)){
                Resources::AddResource(r.second,r.first);
            }
            if(!Resources::Allocated(r.first)){
                Resources::Allocate(r.first);
            }
        }
    }
    virtual void PostInit(){}
    virtual void reloadSettings() = 0;
    virtual void Update(const float dt) = 0;
    virtual void DeInit() = 0;
    virtual ~Scene(){};
};