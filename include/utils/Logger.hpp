#pragma once
#include <string>
#include <ctime>
#include <chrono> 
#include <queue>
#include <iostream>

using namespace std;
//Global logger class (singleton), call write after logXXXX functions
class Logger{//TODO upgrade to atomic variables and other fancy multithreaded stuff
    public:
    enum Level : uint8_t{
        L_ERROR,
        L_INFO,
        L_WARNING
    };

    static Logger& getInstance(){
        static Logger instance;
        return instance;
    }
    //Returns string containing current time in %T format
    static string now(){
        time_t t = time(0);
        char cstr[9];
        strftime(cstr, sizeof(cstr), "%H:%M:%S", localtime(&t)); // %T givse error with mingw
        return cstr;
    }
    //Log string with error level, name wrappers
    static void inline log(string msg,Level l){getInstance().push(msg,l);}
    static void inline logERROR(string msg){log(msg,Level::L_ERROR);}
    static void inline logINFO(string msg){log(msg,Level::L_INFO);}
    static void inline logWARNING(string msg){log(msg,Level::L_WARNING);}

    //Writes all queued messages to output, empties queue
    static void write(){
        Logger& l = getInstance();
        if(!l.lock){
            l.lock = true;
            while(!l.q.empty()){l.pop();}
            l.flush();
            l.lock = false;
        }
    }

    private:
    string levels[3] = {"ERROR", "INFO", "WARNING"};
    //Return header for a logger message
    string header(Level l){return "["+levels[l]+"|"+Logger::now()+"]";}
    //Queue a message
    void inline push(string msg, Level l){q.push(header(l)+" "+msg+"\n");}
    //Write a message to output and dequeue
    void inline pop(){cout << q.front();q.pop();}
    //Flush output
    void inline flush(){cout.flush();}
    bool lock = false; //TODO safer lock
    queue<string> q;
    Logger() = default;
    ~Logger() = default;
    Logger(const Logger&) = delete;
    Logger operator&(const Logger&) = delete;
};