#pragma once
#include "utils/Resources.hpp"

struct Options{};

//Parent class for engines, using singleton to allow future multiple instances
class Engine{
    public:
    template<class T> static T& getInstance(){
        static T instance;
        return instance;
    }
    //TODO Variadic template
    //Start and Stop functions of the engine
    //Starts the engine or reinitializes it with new options
    template<class T> static void Start(Options o){if(getInstance<T>().Inited()){getInstance<T>().ReInit(o);}else{getInstance<T>().Init(o);}}
    //Stops the engine
    template<class T> static void Stop(Options o){if(getInstance<T>().Inited()){getInstance<T>().DeInit(o);}}

    //Functions for initialization, deinitialization and applying new options
    virtual void Init([[maybe_unused]] Options& o) = 0;
    virtual void DeInit([[maybe_unused]] Options& o) = 0;
    virtual void ReInit([[maybe_unused]] Options& o) = 0;
    virtual bool Inited(){return false;}
};