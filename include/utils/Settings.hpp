#pragma once
#include <stdint.h>
#include <vector>
#include <string>
#include <sstream>
#include <unordered_map>
#include "platform/Filesystem.hpp"

using namespace std;

//Global class, singleton, stores strings and provides functions/enums for easy access
//Modifying values sets dirty bit
class Settings{
    public:
    //Default variables
    enum Types : uint8_t{
        FPS,
        SCREEN_WIDTH,
        SCREEN_HEIGHT,
        MEDIA_PATH,
        FONT,
        MUSIC,
        VOLUME,
        TEXTURE_PLAYER,
        TEXTURE_MAP,
        MUSIC_ON,
        DEBUG_LEVEL,
        TYPES_TOTAL
    };
    static Settings& getInstance(){//TODO: static functions
        static Settings instance;
        return instance;
    }
    //Make sure to insert the entry first, if index > TYPES_TOTAL
    static const string& get(size_t i) {return getInstance()[i];}
    static void set(size_t i, const string& s) {getInstance()[i] = s;}

    //inserts an entry, returns it's index
    size_t insert(string&& entry){db.push_back(entry);return db.size()-1;}
    bool registerName(const string&& name,const size_t index){return names.insert({name,index}).second;}
    size_t getIndex(const string&& name){const auto it = names.find(name);return it == names.end() ? 0 : it->second;}

    //Bit array
    //0-8 - Debug level
    //TODO: rework and remove game-specific code
    //Get debug level value (first 8 bit of DEBUG)
    static uint8_t getDEBUG_LEVEL(){return getDEBUG(0,8);}
    static uint64_t getDEBUG(){return stol(get(DEBUG_LEVEL));}
    //Get i-th bit
    static bool getDEBUG(size_t i){return (stol(get(DEBUG_LEVEL)) & 1ULL<<i) != 0ULL;}
    //Get j bits from i
    static uint64_t getDEBUG(size_t i, size_t j){return (stol(get(DEBUG_LEVEL))>>i) & ((1ULL<<j) - 1ULL);}

    template <typename T>
    static T getAs(size_t i) {
        stringstream ss{get(i)};
        T t;
        ss >> t;
        return t;
    }
    template <typename T>
    static T getAs(const string s) {
        return getAs<T>(getInstance().getIndex(string(s)));
    }
    

    //Returns value of dirty bit and sets it to false
    static bool unsetDirty(){bool tmp = getInstance().dirty;getInstance().dirty = false; return tmp;}
    //Utils
    //String to bool
    static bool stob(const string& temp){return temp.compare("0") && temp.compare("false");}
    //
    private: 
    bool dirty = true;
    const string& operator[](size_t i) const {return db[i];}
          string& operator[](size_t i)       {dirty=true;return db[i];}

    vector<string> db = vector<string>(TYPES_TOTAL);
    unordered_map<string,const size_t> names{{"FPS",FPS}};//TODO: more predefined names
    Settings() = default;
    ~Settings() = default;
    Settings(const Settings&) = delete;
    Settings operator&(const Settings&) = delete;
};

template<> uint32_t Settings::getAs(size_t i){return stoi(Settings::get(i));}
template<> uint64_t Settings::getAs(size_t i){return stol(Settings::get(i));}
template<> bool Settings::getAs(size_t i){return stob(Settings::get(i));}
template<> fs::path Settings::getAs(size_t i){return Settings::get(i);}