#pragma once
#include <map>
#include <functional>
#include <thread>

#include <inttypes.h>
//Possible bug with including inttypes
//Workaround:
#ifdef  _WIN32
#define PRIu64 "I64u"
#endif

#include "imgui/imgui.h"
#include "imgui/backends/imgui_impl_sdlrenderer2.h"
#include "imgui/backends/imgui_impl_sdl2.h"
#include "imgui/misc/cpp/imgui_stdlib.h"
#include "implot/implot.h"

#include "utils/Timer.hpp"
#include "utils/Scene.hpp"
#include "utils/Settings.hpp"
#include "utils/Logger.hpp"
#include "utils/Resources.hpp"

using SceneID = size_t;

using namespace std;

//How many timers needed to time different phases of main loop
#define TIMERS 4



//Graph of scenes
//Scenes have an ID stored in a map<SceneID,Scene*>
//Edges are mapped to arbitrary function pointers stored in a map of pairs : map<pair<SceneID,SceneID>, void(*)(Scene* current,Scene* new)>
//Ticks are stored in uint64_t
class SceneGraph{
    public:
    SceneGraph():
        consoleFunctions(),
        ticks(0),
        timer()
    {}
    //Add scenes
    void Add(Scene* scene){scenes.emplace(scene->id,scene);}
    //Add transition functions (optional)
    void Add(SceneID from,SceneID to,void (*f)(Scene*,Scene*)){changes.emplace(make_pair(from,to),f);}
    //Game loop
    void Loop(SceneID start){
        //Storage for timer log
        char buf[120];
        //Sum of extra time for logging
        std::int64_t extra_time_sum = 0;

        //Imgui initialization
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImPlot::CreateContext();
        ImGuiIO& io = ImGui::GetIO(); (void)io;
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
        // Setup Dear ImGui style
        ImGui::StyleColorsDark();
        ImGui_ImplSDL2_InitForSDLRenderer(RenderEngine::getWindow(), RenderEngine::getRenderer());
        ImGui_ImplSDLRenderer2_Init(RenderEngine::getRenderer());

        //Set current scene to start from parameter
        currentScene = scenes[start];
        currentScene->Init();
        while(running){

            /**************************/timer.StopAndStart<3,0>();/**************************/

            const uint64_t FPS = Settings::getAs<uint64_t>(Settings::FPS);
            const uint64_t nsPerTick= 1000000000ul/FPS;
            int mouseX, mouseY;
            [[maybe_unused]] int wheel = 0;
            SDL_Event e;


            /**************************/timer.StopAndStart<0,1>();/**************************/

            //Input handling
            while(SDL_PollEvent(&e) != 0){
                ImGui_ImplSDL2_ProcessEvent(&e);
                if(io.WantCaptureMouse && (e.type == SDL_MOUSEWHEEL || e.type == SDL_MOUSEBUTTONUP || e.type == SDL_MOUSEBUTTONDOWN)){continue;}
                if(e.type == SDL_QUIT){//Exit loop on QUIT event
                    running = false;
                }else if(e.type == SDL_WINDOWEVENT && e.window.event == SDL_WINDOWEVENT_RESIZED){//Resize window, update settings
                    Settings::set(Settings::SCREEN_WIDTH,to_string(e.window.data1));
                    Settings::set(Settings::SCREEN_HEIGHT,to_string(e.window.data2));
                    io.DisplaySize.x = static_cast<float>(e.window.data1);
                    io.DisplaySize.y = static_cast<float>(e.window.data2);
                }else if(e.type == SDL_MOUSEWHEEL){
                    wheel = e.wheel.y;
                }
                if(io.WantCaptureKeyboard && !(e.type == SDL_MOUSEWHEEL || e.type == SDL_MOUSEBUTTONUP || e.type == SDL_MOUSEBUTTONDOWN)){continue;}
                //Add events to current scene buffer
                currentScene->events.emplace_back(e);
            }
            //ImGui input
            const int buttons = SDL_GetMouseState(&mouseX,&mouseY);
            io.MousePos = ImVec2(static_cast<float>(mouseX), static_cast<float>(mouseY));
            io.MouseDown[0] = buttons & SDL_BUTTON(SDL_BUTTON_LEFT);
            io.MouseDown[1] = buttons & SDL_BUTTON(SDL_BUTTON_RIGHT);


            //Reload settings
            if(Settings::unsetDirty()){currentScene->reloadSettings();}

            
            //RenderGUI
            ImGui_ImplSDLRenderer2_NewFrame();
            ImGui_ImplSDL2_NewFrame();
            ImGui::NewFrame();
            
            //Clear
            SDL_SetRenderDrawColor(RenderEngine::getRenderer(),255,255,255,255);//TODO to RenderEngine
            SDL_RenderClear(RenderEngine::getRenderer());
            
            /**************************/timer.StopAndStart<1,2>();/**************************/

            //Update ECS
            float dt = timer.totalD<ratio<1,1>>(TIMERS);
            timer.Stop<TIMERS>(); //Restart wouldnt work because saved time is needed
            timer.Start<TIMERS>();
            io.DeltaTime = dt;
            currentScene->Update(dt);

            /**************************/timer.StopAndStart<2,3>();/**************************/

            
            //Clear input queue
            currentScene->events.clear();

            //Render
            RenderEngine::Render();
            ImGui::Render();
            ImGui_ImplSDLRenderer2_RenderDrawData(ImGui::GetDrawData());
            SDL_RenderPresent(RenderEngine::getRenderer());

            //DELAY to limit fps  //TODO: no busy timer
            std::int64_t extra_time = (std::int64_t)nsPerTick-(std::int64_t)timer.total<nano>(0);
            if(extra_time > 0){
                extra_time_sum += extra_time;
                timer.delay(extra_time);
            }
            //this_thread::sleep_for(chrono::duration<double,nano>(nsPerTick-timer.total<nano>(0)));
            
            //Log once per second
            //TIMER phases, ticks elapsed, average FPS and total time taken in ns
            if(++ticks % FPS == 0){
                uint64_t clt = timer.get<nano>(0) / FPS;
                uint64_t it = timer.get<nano>(1) / FPS;
                uint64_t logt = timer.get<nano>(2) / FPS;
                uint64_t rt = (timer.get<nano>(3)-extra_time_sum) / FPS;
                uint64_t tt = timer.get<nano>(TIMERS) / FPS;
                float elapsed = timer.getD<nano>(TIMERS);
                float fpsavg = (FPS*1000000000ul)/(elapsed);
                snprintf(buf,sizeof(buf),
                    "CL:%012" PRIu64 
                    " IN:%012" PRIu64 
                    " LOG:%012" PRIu64 
                    " REN:%012" PRIu64 
                    " TICKS:%06" PRIu64 
                    " FPS:%08.04f"
                    " NS:%012" PRIu64 ,
                    clt,it,logt,rt,ticks,fpsavg,tt
                );
                //Maybe in the future ;(
                //string s = format("EL:{:0>14} TICKS:{:0>6} TICKTIME:{:0>7.4f} NS: {:0>12}",total,ticks,(float)ticks/((float)(startTime-time)/1000.0f),t1);
                if(Settings::getDEBUG_LEVEL() > 0U || true){
                    Logger::logINFO(string(buf));
                }
                Logger::write();
                
                timer.EraseAll();
                extra_time_sum = 0;
            }
        }
        //Loop exited, deinit scene and free some resources
        currentScene->DeInit();
        ImGui_ImplSDLRenderer2_Shutdown();
        ImGui_ImplSDL2_Shutdown();
        ImPlot::DestroyContext();
        ImGui::DestroyContext();
    }
    //Scenes will call this (through ChangeScene())
    void Change(SceneID toSceneID){
        //Logger::logINFO("Changing scene to "+to_string(toSceneID));

        //Try to change current scene to toScene, call edge function and init/deinit if found
        if(scenes.contains(toSceneID)){
            Scene* toScene = scenes[toSceneID];
            currentScene->DeInit();
            pair<SceneID,SceneID> change = make_pair(currentSceneID,toSceneID);
            if(changes.contains(change)){
                changes[change](currentScene,toScene);
            }
            toScene->Init();
            currentSceneID = toSceneID;
            currentScene = toScene;
        }else{
            //Error, toSceneID not found
        }
    }
    //Quit (callable from other threads / using pointer)
    void Quit(){running = false;}

    SceneID currentSceneID;
    Scene* currentScene;
    map<SceneID,Scene*> scenes;
    map<pair<SceneID,SceneID>,void (*)(Scene*,Scene*)> changes;
    map<string,pair<function<void(vector<string>&)>,size_t>> consoleFunctions;
    uint64_t ticks;
    PerfTimer<TIMERS+1> timer;
    volatile bool running = true;
    //Calls delete on all stored scenes, assumes they were created with new
    void DeleteScenes(){for(auto& scene : scenes){delete scene.second;}}
};


//Forward declaration trick
void ChangeScene(SceneGraph* graph, SceneID to){
    graph->Change(to);
}